#version 330

struct Material
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct PointLight
{
    vec3 pos;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    vec3 attenuation;
};

struct SpotLight
{
    PointLight base;
    vec3 direction;
    float icutoff;
    float ocutoff;
};

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

//in vec3 vpos;
in vec3 normal;
in vec2 tex_coord;

in vec3 wpos;

uniform bool light_object;
uniform bool textured;
uniform sampler2D tex;
uniform sampler2D nmap;
uniform sampler2D smap;
uniform PointLight light = PointLight(vec3(0,1,5),vec3(1),vec3(1),vec3(1), vec3(1,0,0));
uniform vec3 camera;
uniform Material material = Material(vec3(0),vec3(0.5,0,0),vec3(0.7,0.6,0.6),0.25);
uniform int fcounter;

out vec4 main_out;
out vec4 hdr;

// World space coordinates
// normal is plane one ?
mat3 cotangent_frame(vec3 normal, vec3 pos, vec2 uv)
{
    vec3 dp1 = dFdx(pos);
    vec3 dp2 = dFdy(pos);
    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);

    vec3 dp2perp = cross(dp2, normal);
    vec3 dp1perp = cross(normal, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, normal);
}

vec4 phong(bool blinn, vec3 norm, float light_int)
{
    vec3 diff_color = material.diffuse;
    if(textured) diff_color = texture(tex, tex_coord).rgb;

    vec3 ambient_color = material.ambient;
    if(textured) ambient_color = diff_color * 0.02;
    vec3 ambient = ambient_color * light.ambient;
    if(light_int == 0) return OEXT34(ambient);

    vec3 light_dir = normalize(light.pos - wpos);

    float diff = max(dot(norm, light_dir), 0);
    vec3 diffuse = diff_color*light.diffuse*diff * light_int;

    vec3 spec_color = material.specular;
    if(textured) spec_color = texture(smap, tex_coord).rgb;
    vec3 view_dir = normalize(camera - wpos);
    float dot_prod = 0;
    if(blinn)
    {
        vec3 half_dir = normalize(light_dir + view_dir);
        dot_prod = dot(norm, half_dir);
    }
    else
    {
        vec3 refl_dir = reflect(-light_dir, norm);
        dot_prod = dot(view_dir, refl_dir);
    }
    float spec = pow(max(dot_prod, 0), material.shininess*128);
    vec3 specular = spec_color * light.specular * spec * light_int;

    return OEXT34(diffuse + ambient + specular);
}

float plight_int(PointLight light)
{
    float light_dist = distance(light.pos, wpos);
    float attenuation = light.attenuation.x + light_dist*light.attenuation.y + light_dist*light_dist*light.attenuation.z;
    return 1/attenuation;
}

float slight_int(SpotLight light)
{
    vec3 light_dir = normalize(light.base.pos - wpos);
    float theta = dot(light_dir, normalize(-light.direction));
    //float ocutoff = 0.82;
    //float icutoff = 0.95;
    float epsilon = light.icutoff - light.ocutoff;
    return clamp(((theta-light.ocutoff)/epsilon), 0, 1) * plight_int(light.base);
}

void main()
{
    /*if(distance(gl_FragCoord.xy, vec2(400,400))<20)
    {
        if((fcounter%240)<120) main_out = vec4(1,0,0,1);
        else main_out = vec4(0,1,0,1);
        return;
    }*/

    if(light_object)
    {
        main_out = vec4(1,1,1,1);
        hdr = vec4(1,1,1,1);
        return;
    }

    mat3 tbn = cotangent_frame(normal, wpos, tex_coord);
    vec3 norm = texture(nmap, tex_coord).xyz;
    norm = normalize(norm * 2 - 1);
    norm = normalize(tbn*norm);

    main_out = phong(true, norm, plight_int(light));
    if(dot(main_out.rgb, vec3(0.2126,0.7152,0.0722)) > 0.7)
        hdr = main_out;
    else hdr = vec4(0,0,0,1);
}
