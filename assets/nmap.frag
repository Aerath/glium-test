#version 330

struct Material
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct Light
{
    vec3 pos;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

//in vec3 vpos;
in vec3 normal;
in vec2 tex_coord;

in vec3 wpos;

uniform bool textured;
uniform sampler2D tex;
uniform sampler2D nmap;
//uniform vec3 light;
uniform Light light = Light(vec3(0,1,5),vec3(1),vec3(1),vec3(1));
uniform vec3 camera;
uniform vec3 light_mode;
uniform Material material = Material(vec3(0),vec3(0.5,0,0),vec3(0.7,0.6,0.6),0.25);

// World space coordinates
// normal is plane one ?
mat3 cotangent_frame(vec3 normal, vec3 pos, vec2 uv)
{
    vec3 dp1 = dFdx(pos);
    vec3 dp2 = dFdy(pos);
    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);

    vec3 dp2perp = cross(dp2, normal);
    vec3 dp1perp = cross(normal, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, normal);
}

void main()
{
    vec3 light_dir = normalize(light.pos - wpos);

    mat3 tbn = cotangent_frame(normal, wpos, tex_coord);
    vec3 norm = texture(nmap, tex_coord).xyz;
    norm = normalize((norm * 2 - 1));
    norm = normalize(tbn*norm);

    float light_dist = distance(light.pos, wpos);
    float sq_ldist = light_dist * light_dist;

    vec3 diff_color = material.diffuse;
    if(textured) diff_color = texture(tex, tex_coord).rgb;
    float diff = max(dot(norm, light_dir), 0);
    vec3 diffuse = diff_color*light.diffuse*diff / sq_ldist;
    //diffuse *= light_mode.x;

    vec3 ambient = material.ambient * light.ambient;
    //ambient *= light_mode.y;

    /*vec3 cam_dir = normalize(-vpos);
    vec3 half_dir = normalize(nlight + cam_dir);
    float spec = pow(max(dot(half_dir, norm), 0), 16) * 0.4;
    //spec = 0;*/
    /*vec3 specular = vec3(0);
    if(dot(norm, -light) >= 0)
    {
        vec3 refl_dir = reflect(light, norm);
        float shin_fact = dot(refl_dir, normalize(camera));
        if(shin_fact > 0)
        specular = material.specular * light.specular * pow(shin_fact, 8);
    }*/

    vec3 view_dir = normalize(camera - wpos.xyz);
    vec3 refl_dir = reflect(-light_dir, norm);
    float spec = pow(max(dot(view_dir, refl_dir), 0), material.shininess*128);
    vec3 specular = material.specular * light.specular * spec / sq_ldist;

    gl_FragColor = OEXT34(diffuse + ambient + specular);
}
