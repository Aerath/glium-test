#version 330

//const float amb_pwr = 0.1f;
//const float spec_pwr = 0.5f;
//const vec3 amb_color = vec3(0.2,0,0);
const vec3 light_acolor = vec3(1,1,1);
const vec3 light_dcolor = vec3(1,1,1);
const vec3 light_scolor = vec3(1,1,1);

//in vec3 vpos;
in vec3 normal;
in vec2 tex_coord;

in vec3 light_dir; // camera space
in vec3 wpos;      // vertex in world space
in vec3 eye_dir;   // camera space

uniform bool textured;
uniform sampler2D tex;
uniform sampler2D nmap;
uniform vec3 light;
uniform vec3 camera;
uniform vec3 light_mode;

mat3 cotangent_frame(vec3 normal, vec3 pos, vec2 uv)
{
    vec3 dp1 = dFdx(pos);
    vec3 dp2 = dFdy(pos);
    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);

    vec3 dp2perp = cross(dp2, normal);
    vec3 dp1perp = cross(normal, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, normal);
}

/*void old_main()
{
    vec3 color;

    if(textured)
    {color = texture(tex, tex_coord).rgb;}
    else color = vec3(1,0,0);

    vec3 light_dir = normalize(light - vpos);
    //vec3 cam_dir = normalize(camPos - vpos);
    vec3 half_dir = normalize(normalize(light) + normalize(-vpos));
    vec3 reflectDir = reflect(-light, normal);

    float diff = max(dot(normal, normalize(light)), 0);
    //float spec = pow(dot(cam_dir, refec_dir), 128) * spec_pwr;
    float spec = pow(max(dot(half_dir, normalize(normal)), 0), 16);

    //vec3 lightning = light_color * (amb_pwr + diff + spec);
    vec3 lightning = amb_color + diff*diff_color + spec*spec_color;

    color = color * lightning;
    gl_FragColor = vec4(color,1);
}*/

void main()
{
    vec3 color;

    if(textured) color = texture(tex, tex_coord).rgb;
    else color = vec3(1,0,0);
    vec3 amb_color = color;
    vec3 diff_color = color;
    vec3 spec_color = color;

    mat3 tbn = cotangent_frame(normal, wpos, tex_coord);
    vec3 mapped_norm = texture(nmap, tex_coord).xyz;
    vec3 t_norm = normalize((mapped_norm*2 - 1));
    vec3 t_light_dir = tbn * light_dir;
    vec3 t_eye_dir = tbn * eye_dir;
    t_norm = normal;
    t_light_dir = normalize(light-wpos.xyz);
    t_eye_dir = eye_dir;

    float light_dist = distance(light, wpos);
    float sq_ldist = light_dist * light_dist;
    //sq_ldist = 1;

    float diff = max(dot(t_norm, t_light_dir), 0);
    vec3 diffuse = diff_color*light_dcolor*diff / sq_ldist;
    //diffuse *= light_mode.x;

    vec3 ambient = amb_color * light_acolor * 0.1;
    //ambient *= light_mode.y;

    /*vec3 cam_dir = normalize(-vpos);
    vec3 half_dir = normalize(nlight + cam_dir);
    float spec = pow(max(dot(half_dir, t_norm), 0), 16) * 0.4;
    //spec = 0;*/
    /*vec3 specular = vec3(0);
    if(dot(t_norm, -light) >= 0)
    {
        vec3 refl_dir = reflect(light, t_norm);
        float shin_fact = dot(refl_dir, normalize(camera));
        if(shin_fact > 0)
        specular = spec_color * light_scolor * pow(shin_fact, 8);
    }*/

    vec3 view_dir = normalize(camera - wpos.xyz);
    vec3 refl_dir = reflect(-t_light_dir, t_norm);
    //float spec = pow(max(dot(t_eye_dir, refl_dir), 0), 64);
    float spec = pow(max(dot(view_dir, refl_dir), 0), 64);
    vec3 specular = spec_color * light_scolor * spec / sq_ldist;

    color = diffuse + ambient + specular;
    //color = specular;
    //color = amb_factor*amb_color + color*diff_color*diff + spec*spec_color;
    gl_FragColor = vec4(color,1);
}
