#version 150

struct Material
{
#ifdef PHONG
	vec3 ambient;
	vec3 albedo;
	vec3 specular;
	float shininess;
#endif
#ifdef PBR
	vec3 albedo;
	vec4 albedoFactor;
	float metallic;
	float metallicFactor;
	float roughness;
	float roughFactor;
	vec3 emissiveColor;
	vec3 emissiveFactor;
	float occlusionFactor;
	int alpha_mode;

#define ALPHA_MODE_OPAQUE 0
#define ALPHA_MODE_MASK 1
#define ALPHA_MODE_BLEND 2

#endif
};

#ifdef PHONG
uniform Material material = Material(vec3(0),vec3(0.5,0,0),vec3(0.7,0.6,0.6),0.25);
#endif
#ifdef PBR
uniform Material material = Material(vec3(1),vec4(1), 1,1, 1,1, vec3(1),vec3(1), 1, ALPHA_MODE_OPAQUE);
#endif

#ifdef USE_GEOM_SHAD
#define VS_OUT GS_OUT
#endif

in VS_OUT
{
	vec3 wpos;
	vec3 normal;
	vec2 tex_coord;
};

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

uniform bool textured = false;
uniform sampler2D albedotex;

void main()
{
	vec4 color;
	if(textured) color = texture(albedotex, tex_coord);
	else color = vec4(material.albedo, 1);
#ifdef PBR
	color *= material.albedoFactor;
#endif
	if(color.a <= 0.1) discard;
	gl_FragColor = color;
}
