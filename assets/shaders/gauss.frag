#version 130

in vec2 tex_coord;

uniform int fcounter;
uniform sampler2D image;
uniform bool horizontal;
uniform float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);
//uniform float weight[5] = float[] (0.382928, 0.241732, 0.060598, 0.005977, 0.000229); // From dev.theomader.com/gaussian-kernel-calculator

#ifndef STEP
#define STEP 1
#endif
#define PIX_STEP float(uint(1))

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

vec3 gblur(sampler2D image, vec2 uv, vec2 dir)
{
    dir /= textureSize(image, 0); // get size of single texel

    vec3 res = texture(image, uv).rgb * weight[0];
    for(int i=1; i<5; i++)
    {
        float f = PIX_STEP * float(i);
        res += texture(image, uv + f*dir).rgb * weight[i];
        res += texture(image, uv - f*dir).rgb * weight[i];
    }

    return res;
}

void main()
{
    if(horizontal) gl_FragColor = OEXT34(gblur(image, tex_coord, vec2(1,0)));
    else gl_FragColor = OEXT34(gblur(image, tex_coord, vec2(0,1)));
}
