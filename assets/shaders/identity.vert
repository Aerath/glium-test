#version 130

in vec3 pos;
in vec2 tex;

out vec2 tex_coord;

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

void main()
{
    tex_coord = tex;

    gl_Position = OEXT34(pos);
}
