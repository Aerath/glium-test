#version 150

in vec3 pos;	//
in vec3 norm;   // model space
in vec2 tex;	//

out VS_OUT
{
	vec3 wpos;
	vec3 normal;
	vec2 tex_coord;
};
//out vec3 light_dir; // camera space
//out vec3 eye_dir;   // camera space

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
//uniform vec3 camera;
uniform int fcounter;

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

void main()
{
	//mat3 normalMatrix = mat3(transpose(inverse(view*model)));
	//normal = normalize((proj*ZEXT34(normalMatrix*norm)).xyz);
	normal = normalize((transpose(inverse(model)) * OEXT34(norm)).xyz);
	//normal = norm;

	tex_coord = tex;

	vec4 mpos = model * OEXT34(pos);
	wpos = mpos.xyz;
	gl_Position = proj * view * mpos;

	//eye_dir = normalize(-vpos.xyz);
	//light_dir = normalize((view * vec4(light,1)).xyz + eye_dir);
}
