#version 130

in vec2 tex_coord;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D alpha;
uniform float exposure = 1;
uniform int fcounter;

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

// Blur kernel
/*const float kern_off = 1.0/300.0;
const vec2 kernel_off[9] = vec2[](
		vec2(-kern_off, kern_off),
		vec2(0,		 kern_off),
		vec2(-kern_off, kern_off),
		vec2(-kern_off, 0),
		vec2(0,		 0),
		vec2(-kern_off, 0),
		vec2(-kern_off, kern_off),
		vec2(0,		 kern_off),
		vec2(-kern_off, kern_off)
	);
const float kernel[9] = float[](
		1.0/16.0, 2.0/16.0, 1.0/16.0,
		2.0/16.0, 4.0/16.0, 2.0/16.0,
		1.0/16.0, 2.0/16.0, 1.0/16.0
	);*/

vec3 gamma_corr(vec3 color, float gamma) // tbd after tone mapping
{
	return pow(color, vec3(1/gamma));
}

vec3 expos_tone_map(vec3 color, float expos)
	// expos=1 seems to be the neutral value
{
	return vec3(1) - exp(-color*expos);
}

vec3 reinhard_tone_map(vec3 color)
{
	color /= (color + vec3(1));
	return color;
}

void main()
{
	vec3 color = texture(tex1, tex_coord).rgb/* + texture(tex2, tex_coord).rgb*/;
	vec4 acolor = texture(alpha, tex_coord);
	color = mix(color, acolor.rgb, acolor.a) + texture(tex2, tex_coord).rgb;
	color = reinhard_tone_map(color);
	//color = gamma_corr(color, exposure);
	gl_FragColor = OEXT34(color);
}
