#version 330

layout (triangles) in;
layout (triangle_strip, max_vertices=3) out;

uniform int fcounter;

in VS_OUT
{
    vec3 wpos;
    vec3 normal;
    vec2 tex_coord;
} vs_out[];

out GS_OUT
{
    vec3 wpos;
    vec3 normal;
    vec2 tex_coord;
} gs_out;

vec3 comp_norm()
{
    vec4 a = gl_in[0].gl_Position - gl_in[1].gl_Position;
    vec4 b = gl_in[2].gl_Position - gl_in[1].gl_Position;
    return normalize(cross(vec3(a), vec3(b)));
}

vec4 explode(vec4 pos, vec3 norm)
{
    return pos + (0.5 + 0.5 * sin(fcounter/60.0))*0.1*vec4(norm, 1);
}

void main()
{
    vec3 normal = comp_norm();

    for(int i=0; i<3; i++)
    {
        //gl_Position = explode(gl_in[i].gl_Position, normal);
        gl_Position = gl_in[i].gl_Position;
        gs_out.wpos = vs_out[i].wpos;
        gs_out.normal = vs_out[i].normal;
        gs_out.tex_coord = vs_out[i].tex_coord;
        EmitVertex();
    }
}
