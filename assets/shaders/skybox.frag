#version 130

uniform samplerCube cubemap;

in vec3 tex_coord;

void main()
{
	vec3 color = texture(cubemap, tex_coord).rgb;
	//color = pow(color, vec3(2.2));
	gl_FragColor = vec4(color,1);
}
