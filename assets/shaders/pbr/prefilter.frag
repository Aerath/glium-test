#version 130

const float PI = 3.1415926535893638;

const uint SAMPLE_COUNT = 1024u;

in vec3 tex_coord;

uniform samplerCube cubemap;
uniform float roughness;

// Used to generate Hammersley sequences
float RadicalInverse_VdC(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

// Generates a low-discrepancy sample (i) among a possible sample set of size N
vec2 Hammersley(uint i, uint N)
{
	return vec2(float(i)/float(N), RadicalInverse_VdC(i));
}

// Generates samples generally oriented along the normal,
// spread according to roughness
vec3 ImportanceSampleGGX(vec2 xi, vec3 normal, float a2)
{
	float phi = 2*PI * xi.x;
	float cosTheta = sqrt((1-xi.y) / (1 + (a2-1)*xi.y));
	float sinTheta = sqrt(1 - cosTheta*cosTheta);

	vec3 H = vec3(
			cos(phi) * sinTheta,
			sin(phi) * sinTheta,
			cosTheta
			);

	vec3 up = abs(normal.z)<0.999 ? vec3(0,0,1) : vec3(1,0,0);
	vec3 tangent = normalize(cross(up, normal));
	vec3 bitangent = cross(normal, tangent);

	vec3 sample_vec = tangent*H.x + bitangent*H.y + normal*H.z;
	return normalize(sample_vec);
}

float DistributionGGX(float NdotH, float a2)
{
	float NdotH2 = NdotH*NdotH;

	float denom = (NdotH2 * (a2 - 1) + 1);
	denom = PI * denom * denom;

	return a2 / denom;
}

void main()
{
	float a = roughness * roughness;
	float a2 = a * a;

	vec3 normal = normalize(tex_coord);
	// we assume V == R == normal

	float totalWeight = 0;
	vec3 result = vec3(0);

	for(uint i=0u; i<SAMPLE_COUNT; ++i)
	{

		vec2 xi = Hammersley(i, SAMPLE_COUNT);
		vec3 H = ImportanceSampleGGX(xi, normal, a2);
		float NdotH = max(dot(normal, H), 0);
		vec3 L = normalize(2 * NdotH * H - normal);

		float NdotL = max(dot(normal, L), 0);
		if(NdotL > 0)
		{
			float HdotV = max(dot(H, normal), 0);
			float D = DistributionGGX(NdotH, a2);
			float pdf = D * NdotH / (4*HdotV);

			float res = 512;
			float saTexel = 4*PI / (6*res*res);
			float saSample = 1 / (SAMPLE_COUNT * pdf + 0.0001);
			float mipLevel = roughness==0 ? 0 : 0.5 * log2(saSample / saTexel);

			result += textureLod(cubemap, L, mipLevel).rgb * NdotL;
			totalWeight += NdotL;
		}
	}

	gl_FragColor = vec4(result/totalWeight, 1);
}
