#version 130

const float PI = 3.141592653589793638;

const uint SAMPLE_COUNT = 1024u;

in vec2 tex_coord;

// Used to generate Hammersley sequences
float RadicalInverse_VdC(uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

// Generates a low-discrepancy sample (i) among a possible sample set of size N
vec2 Hammersley(uint i, uint N)
{
	return vec2(float(i)/float(N), RadicalInverse_VdC(i));
}

// Generates samples generally oriented along the normal,
// spread according to roughness
vec3 ImportanceSampleGGX(vec2 xi, vec3 normal, float a2)
{
	float phi = 2*PI * xi.x;
	float cosTheta = sqrt((1-xi.y) / (1 + (a2-1)*xi.y));
	float sinTheta = sqrt(1 - cosTheta*cosTheta);

	vec3 H = vec3(
			cos(phi) * sinTheta,
			sin(phi) * sinTheta,
			cosTheta
			);

	vec3 up = abs(normal.z)<0.999 ? vec3(0,0,1) : vec3(1,0,0);
	vec3 tangent = normalize(cross(up, normal));
	vec3 bitangent = cross(normal, tangent);

	vec3 sample_vec = tangent*H.x + bitangent*H.y + normal*H.z;
	return normalize(sample_vec);
}

float geomSchlickGGX(float a, float NdotX)
{
	float k = a / 2;

	return NdotX / (k + NdotX*(1-k));
}

float geomSmith(float a, float NdotV, float NdotL)
{
	return geomSchlickGGX(a, NdotV) * geomSchlickGGX(a, NdotL);
}

vec2 IntegrateBRDF(float NdotV, float roughness)
{
	float a = roughness * roughness;
	float a2 = a * a;

	vec3 V = vec3(
			sqrt(1 - NdotV*NdotV),
			0,
			NdotV
			);

	vec3 N = vec3(0,0,1);

	float A = 0;
	float B = 0;

	for(uint i=0u; i<SAMPLE_COUNT; i++)
	{
		vec2 xi = Hammersley(i, SAMPLE_COUNT);
		vec3 H = ImportanceSampleGGX(xi, N, a2);
		vec3 L = normalize(2 * dot(V, H) * H - V);

		float NdotL = max(dot(N, L), 0);
		if(NdotL > 0)
		{
			float NdotH = max(dot(N, H), 0);
			float VdotH = max(dot(V, H), 0);

			float G = geomSmith(a, NdotV, NdotL);
			float G_Vis = G * VdotH / (NdotH * NdotV);
			float Fc= pow(1 - VdotH, 5);

			A+= (1-Fc) * G_Vis;
			B += Fc * G_Vis;
		}
	}

	A /= float(SAMPLE_COUNT);
	B /= float(SAMPLE_COUNT);

	return vec2(A, B);
}

void main()
{
	vec2 integrated = IntegrateBRDF(tex_coord.x, tex_coord.y);
	gl_FragColor = vec4(integrated, 0, 1);
}
