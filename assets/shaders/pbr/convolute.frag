#version 130

uniform samplerCube cubemap;

in vec3 tex_coord;

const float PI = 3.141592653589793638;
const vec3 Y = vec3(0, 1, 0);

const float delta = 0.025;

void main()
{
	//gl_FragColor = texture(cubemap, tex_coord);
	//return;

	vec3 normal = normalize(tex_coord);
	vec3 right = cross(Y, normal);
	vec3 up = cross(normal, right);

	vec3 irradiance = vec3(0);
	uint nSamples = uint(0);
	for(float phi=0; phi<2*PI; phi+=delta)
	for(float theta=0; theta<0.5*PI; theta+=delta)
	{
		vec3 tangent = vec3(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta));
		vec3 sampleDir = tangent.x*right + tangent.y*up + tangent.z * normal;

		irradiance += texture(cubemap, sampleDir).rgb * cos(theta) * sin(theta);
		nSamples++;
	}

	gl_FragColor = vec4(PI * irradiance / float(nSamples), 1);
}
