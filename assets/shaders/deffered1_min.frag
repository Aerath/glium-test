#version 150

out vec4 colortex;
out vec3 postex;
out vec3 normtex;
out vec4 spectex;

#ifdef USE_GEOM_SHAD
#define VS_OUT GS_OUT
#endif

in VS_OUT
{
	vec3 wpos;
	vec3 normal;
	vec2 tex_coord;
};

void main()
{
	colortex = vec4(1, 0, 0, 1);
	postex = vec3(0);
	normtex= vec3(0);
	spectex = vec4(0);
}
