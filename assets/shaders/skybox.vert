#version 130

in vec3 pos;
out vec3 tex_coord;

uniform mat4 proj;
uniform mat4 view;

void main()
{
	tex_coord = pos;

	mat4 view2 = mat4(mat3(view));

	vec4 pos2 = proj*view2 * vec4(pos, 1);

	gl_Position = pos2.xyww;
}
