#version 330

//layout (triangles) in;
layout (triangles) in;
layout (line_strip, max_vertices=6) out;

uniform int fcounter;

in VS_OUT
{
	vec3 wpos;
	vec3 normal;
	vec2 tex_coord;
} vs_out[];

vec3 comp_norm()
{
    vec4 a = gl_in[0].gl_Position - gl_in[1].gl_Position;
    vec4 b = gl_in[2].gl_Position - gl_in[1].gl_Position;
    return normalize(cross(vec3(a), vec3(b)));
}

void gen_line(int idx)
{
	gl_Position = gl_in[idx].gl_Position;
	EmitVertex();
	gl_Position = gl_in[idx].gl_Position + vec4(vs_out[idx].normal/12, 0);
	EmitVertex();
	EndPrimitive();
}

void main()
{
	for(int i=0; i<3; i++)
		gen_line(i);
}
