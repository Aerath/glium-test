#version 150

struct Material
{
	vec3 albedo;
	vec4 albedoFactor;
	float metallic;
	float metallicFactor;
	float roughness;
	float roughFactor;
	vec3 emissiveColor;
	vec3 emissiveFactor;
	float occlusionFactor;
	int alpha_mode;

#define ALPHA_MODE_OPAQUE 0
#define ALPHA_MODE_MASK 1
#define ALPHA_MODE_BLEND 2
};

in VS_OUT
{
	vec3 wpos;
	vec3 normal;
	vec2 tex_coord;
};

uniform bool textured = false;
uniform sampler2D albedotex;
uniform bool ntextured = false;
uniform sampler2D nmap;
uniform bool mrtextured = false;
uniform sampler2D mrmap;
uniform bool emtextured = false;
uniform sampler2D emmap;
uniform bool aotextured = false;
uniform sampler2D aomap;

uniform Material material = Material(vec3(1),vec4(1), 1,1, 1,1, vec3(1),vec3(1), 1, ALPHA_MODE_OPAQUE);

//out vec4 main_out;

//TODO: shading

void main()
{
	if(material.alpha_mode != ALPHA_MODE_BLEND)
	{
		discard;
		return;
	}

	vec4 color;
	if(textured) color = texture(albedotex, tex_coord);
	else color = vec4(material.albedo, 1);
	//color *= material.albedoFactor;
	color *= vec4(material.albedoFactor.rgb, 1);

	if(color.a == 1 || color.a == 0)
	{
		discard;
		return;
	}

	//main_out = color;
  gl_FragColor = color;
}
