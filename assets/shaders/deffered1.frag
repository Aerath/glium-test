#version 150

struct Material
{
#ifdef PHONG
	vec3 ambient;
	vec3 albedo;
	vec3 specular;
	float shininess;
#endif
#ifdef PBR
	vec3 albedo;
	vec4 albedoFactor;
	float metallic;
	float metallicFactor;
	float roughness;
	float roughFactor;
	vec3 emissiveColor;
	vec3 emissiveFactor;
	float occlusionFactor;
	int alpha_mode;

#define ALPHA_MODE_OPAQUE 0
#define ALPHA_MODE_MASK 1
#define ALPHA_MODE_BLEND 2

#endif
};

#ifdef USE_GEOM_SHAD
#define VS_OUT GS_OUT
#endif

in VS_OUT
{
	vec3 wpos;
	vec3 normal;
	vec2 tex_coord;
};

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

uniform bool textured = false;
uniform sampler2D albedotex;
uniform bool ntextured = false;
uniform sampler2D nmap;
#ifdef PHONG
uniform bool stextured = false;
uniform sampler2D smap;
#endif
#ifdef PBR
uniform bool mrtextured = false;
uniform sampler2D mrmap;
uniform bool emtextured = false;
uniform sampler2D emmap;
uniform bool aotextured = false;
uniform sampler2D aomap;

uniform bool alphapass = false;
#endif

#ifdef PHONG
uniform Material material = Material(vec3(0),vec3(0.5,0,0),vec3(0.7,0.6,0.6),0.25);
#endif
#ifdef PBR
//uniform Material material = Material(vec3(0.7, 0.2, 0.1),vec3(0.5,0,0),0.5);
uniform Material material = Material(vec3(1),vec4(1), 1,1, 1,1, vec3(1),vec3(1), 1, ALPHA_MODE_OPAQUE);
#endif

uniform int fcounter;

out vec4 colortex;
out vec4 postex;
out vec4 normtex;
#ifdef PHONG
out vec4 spectex;
#endif
#ifdef PBR
out vec4 mrotex;
out vec4 emtex;
#endif

// Fast TBN from https://www.geeks3d.com/20130122/normal-mapping-without-precomputed-tangent-space-vectors/
mat3 fast_tbn(vec3 normal)
{
  vec3 t; 
  vec3 b; 
  vec3 c1 = cross(normal, vec3(0.0, 0.0, 1.0)); 
  vec3 c2 = cross(normal, vec3(0.0, 1.0, 0.0)); 
  if (length(c1) > length(c2))
    t = c1; 
  else t = c2; 
  t = normalize(t);
  b = normalize(cross(normal, t)); 

  return mat3(t, b, normal);
}

// World space coordinates
// normal is plane one ?
// TODO: dFd[xy] in fact returns dFd[xy]{Coarse,Fine} according to performance settings
// TODO: It can be set by GL_FRAGMENT_SHADER_DERIVATIVE_HINT hint
mat3 cotangent_frame(vec3 normal, vec3 pos, vec2 uv)
{
	vec3 dp1 = dFdx(pos);
	vec3 dp2 = dFdy(pos);
	vec2 duv1 = dFdx(uv);
	vec2 duv2 = dFdy(uv);

	vec3 dp1perp = cross(normal, dp1);
	vec3 dp2perp = cross(dp2, normal);
	vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
	vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

  // FIXME: sometimes (alpha-opaque edges on squid ink bottle does) dot(T,T) and dot(B,B) are both
  // null making the result a null matrix and further use through the normalize func return nan
  // (as normalization of a null vector is not defined)
	float invmax = inversesqrt(max(dot(T, T), dot(B, B)+0.001));
	return mat3(T * invmax, B * invmax, normal);
}

void main()
{
	vec4 color;
	if(textured) color = texture(albedotex, tex_coord);
	else color = vec4(material.albedo, 1);
#ifdef PBR
	color *= material.albedoFactor;
	//if((fcounter%180)<90) color.a = log2(color.a + 1.0);
	//else color.a = log2(9*color.a+1)/log2(10);

	// TODO: Check if all this should really be inside ifdef PBR

	if(alphapass == false)
	{
		if(material.alpha_mode == ALPHA_MODE_OPAQUE)
			colortex = vec4(color.rgb, 1);
		//else if(color.a < 0.9)
		else if(color.a != 1)
		{
			discard;
			return;
		}
		else colortex = color;
	}
	else
	{
		if(material.alpha_mode != ALPHA_MODE_BLEND || color.a==1/* || color.a <= 0.05 || color.a >= 0.95*/)
		{
			discard;
			return;
		}
		else colortex = color;
	}
#endif

	postex = vec4(wpos,1);

	if(ntextured)
	{
		mat3 tbn = cotangent_frame(normal, wpos, tex_coord);
		vec3 norm = texture(nmap, tex_coord).xyz;
		norm = normalize(norm * 2 - 1);
		norm = normalize(tbn*norm);
    normtex = vec4(norm, 1);
	}
	else normtex = vec4(normal,1);

#ifdef PHONG
	vec3 spec = material.specular;
	if(stextured) spec = texture(smap, tex_coord).rgb;
	spectex = vec4(spec, material.shininess);
#endif

#ifdef PBR
	float occlusion = 1;
	if(aotextured)
		occlusion = texture(aomap, tex_coord).r;

	vec3 mro;
	if(mrtextured) mro = vec3(occlusion, texture(mrmap, tex_coord).gb);
	//else mrtex = vec3(0, material.roughness, material.metallic);
	else mro = vec3(occlusion, material.roughness, material.metallic);
	mro *= vec3(material.occlusionFactor, material.roughFactor, material.metallicFactor);
	mrotex = vec4(mro, 1);

	vec3 em;
	if(emtextured) em = texture(emmap, tex_coord).rgb;
	else em = material.emissiveColor;
	em *= material.emissiveFactor;
	emtex = vec4(em, 1);
#endif
}
