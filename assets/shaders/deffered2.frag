#version 130

#ifndef ALPHA_PASS
#define ALPHA_PASS false
#endif

const float PI = 3.141592653589793638;
const float MAX_REFLECTION_LOD = 4;

struct PointLight
{
	vec3 pos;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 attenuation;
};

struct SpotLight
{
	PointLight base;
	vec3 direction;
	float icutoff;
	float ocutoff;
};

#ifdef SOBEL
const mat3 sx = mat3(
    1.0, 2.0, 1.0,
    0.0, 0.0, 0.0,
    -1.0, -2.0, -1.0
);

const mat3 sy = mat3(
    1.0, 0.0, -1.0,
    2.0, 0.0, -2.0,
    1.0, 0.0, -1.0
);


float sobel(sampler2D src, vec2 uv)
{
  vec2 texel_nsize = vec2(3.0) / textureSize(src, 0); // get normalized size of a single texel

  mat3 I;
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
    {
      vec3 sample = texture(src, uv+vec2(i,j)*texel_nsize).rgb/2+0.5;
      I[i][j] = length(sample);
    }

  float gx = 0;
  float gy = 0;
  for (int i=0;i<3; i++)
  {
    gx += dot(sx[i], I[i]);
    gy += dot(sy[i], I[i]);
  }
  return length(vec2(gx,gy));
}
#endif

#ifdef MAD
#define ZEXT34(vec) ((vec).xyzz*constants.xxxy)
#define OEXT34(vec) ((vec).xyzz*constants.xxxy+constants.yyyx)
const vec2 constants = vec2(1,0);
#else
#define ZEXT34(vec) vec4(vec, 0)
#define OEXT34(vec) vec4(vec, 1)
#endif

in vec2 tex_coord;

uniform sampler2D coltex;
uniform sampler2D postex;
uniform sampler2D normtex;
#ifdef PHONG
uniform sampler2D spectex;
#endif
#ifdef PBR
uniform sampler2D mrotex;
uniform sampler2D emtex;
#ifdef IBL
uniform samplerCube irrmap;
uniform samplerCube envmap;
uniform sampler2D brdf_int;
#endif
#endif
uniform sampler2D shmap;
uniform PointLight light = PointLight(vec3(0,1,5),vec3(1),vec3(1),vec3(1), vec3(1,0,0));
uniform mat4 lspace;
uniform vec3 camera;
uniform int fcounter;
uniform bool bloom_needed = false;

out vec4 main_out;
out vec4 hdr;

#define SHADOW_SAMPLING_OFFSET 2
float shadow(vec3 fragpos, float bias)
{
	//return 1.0;
	vec4 pos_lspace4 = lspace*OEXT34(fragpos);
	vec3 pos_lspace = pos_lspace4.xyz / pos_lspace4.w;
	pos_lspace = pos_lspace * 0.5 + 0.5;
	if(pos_lspace.z > 1) return 1.0; // No shadow beyond far clipping pane

	vec2 texelSize = 1.0 / textureSize(shmap, 0);
	float shadow = 0.0;
	for(int x=-SHADOW_SAMPLING_OFFSET; x<=SHADOW_SAMPLING_OFFSET; x++)
		for(int y=-SHADOW_SAMPLING_OFFSET; y<=SHADOW_SAMPLING_OFFSET; y++)
		{
			float closestDepth = texture(shmap, pos_lspace.xy + vec2(x,y)*texelSize).r;
			shadow += pos_lspace.z-bias > closestDepth ? 0.0 : 1.0;
		}
	return shadow / pow(SHADOW_SAMPLING_OFFSET*2+1, 2);
}

float plight_int(PointLight light, vec3 fragpos)
{
	float light_dist = distance(light.pos, fragpos);
	light_dist *= 0.1;
	//float attenuation = light.attenuation.x + light_dist*light.attenuation.y + light_dist*light_dist*light.attenuation.z;
	vec3 light_dists = vec3(1, light_dist, light_dist*light_dist);
	//vec3 light_dists = vec3(0, 0, light_dist*light_dist);
	float attenuation = dot(light_dists, light.attenuation);
	//attenuation = light_dist;
	return 1/attenuation;
}

float slight_int(SpotLight light, vec3 fragpos)
{
	vec3 light_dir = normalize(light.base.pos - fragpos);
	float theta = dot(light_dir, normalize(-light.direction));
	//float ocutoff = 0.82;
	//float icutoff = 0.95;
	float epsilon = light.icutoff - light.ocutoff;
	return clamp(((theta-light.ocutoff)/epsilon), 0, 1) * plight_int(light.base, fragpos);
}

#ifdef PHONG
vec4 phong(vec3 pos, float light_int)
{
	vec3 norm = texture(normtex, tex_coord).xyz;
	vec3 col = texture(coltex, tex_coord).rgb;

	vec3 ambient_color = col * 0.02;
	vec3 ambient = ambient_color * light.ambient;
	if(light_int == 0)
		return OEXT34(ambient);

	vec3 light_dir = normalize(light.pos - pos);
	float bias = max(0.0005 * (1.0-dot(norm, light_dir)), 0.000005);
	light_int *= shadow(pos, bias);
	if(light_int == 0)
		return OEXT34(ambient);

	float diff = max(dot(norm, light_dir), 0);
	vec3 diffuse = col*light.diffuse*diff * light_int;

	vec3 view_dir = normalize(camera - pos);
	float dot_prod = 0;
#ifdef BLINN
	vec3 half_dir = normalize(light_dir + view_dir);
	dot_prod = dot(norm, half_dir);
#else
	vec3 refl_dir = reflect(-light_dir, norm);
	dot_prod = dot(view_dir, refl_dir);
#endif

	vec4 spec_data = texture(spectex, tex_coord);
	vec3 spec = spec_data.rgb;
	float shininess = spec_data.a;
	float spec_f = pow(max(dot_prod, 0), shininess*128);
	vec3 specular = spec * light.specular * spec_f * light_int;

	return OEXT34(ambient + diffuse + specular);
}
#endif

#ifdef PBR
/*float sphericalGaussianApprox(float cosX, float modiffSpecPower) // from https://seblagarde.wordpress.com/2011/08/17/hello-world/
{
	return exp2(modiffSpecPower * cosX - modiffSpecPower);
}
#define OneOnLN2_x6 8.656170
// == 1/ln(2) * 6   (6 is SpecularPower of 5 + 1)

vec3 fresnelSchlickOpti(vec3 half_dir, vec3 view_dir, vec3 albedo, float metallic)
{
	vec3 F0 = vec3(0.04);
	F0 = mix(F0, albedo, metallic);

	float cosTheta = max(dot(half_dir, view_dir), 0);

	return F0 + (1-F0) * exp2(-OneOnLN2_x6 * saturate());
}*/

vec3 fresnelSchlick(vec3 H, vec3 V, vec3 F0)
{
	float HdotV = dot(H, V);
	float cosTheta = max(HdotV, 0);

	return F0 + (1-F0) * pow(1-cosTheta, 5);
}

vec3 fresnelSchlickRough(float NdotV, vec3 F0, float roughness)
{
	float cosTheta = NdotV;

	vec3 term2 = max(vec3(1-roughness), F0);
	return F0 + (term2-F0) * pow(1-cosTheta, 5);
}

float distribGGX(vec3 N, vec3 half_dir, float roughness)
{
	float a = roughness*roughness;
	float a2 = a*a;

	float NdotH = max(dot(N, half_dir), 0);
	float NdotH2 = NdotH*NdotH;

	float denom_part = NdotH2 * (a2-1) + 1;
	denom_part = max(denom_part, 0.001);

	return a2 / (PI * denom_part*denom_part);
}

float geomGGX(float roughness, float NdotX)
{
  float a = roughness*roughness;
  return 2*NdotX / (NdotX + length(vec2(a, (1-a*a)*NdotX)));
}

float geomSchlickGGX(float roughness, float NdotX)
{
	/*float r = roughness+1;
  float a = r*r;
	float k = a / 8.0;*/
  float r = roughness;
  float a = r*r;
  float k = a / 2;

	return NdotX / (0.001 + k + NdotX*(1-k));
}

float geomSmith(float roughness, float NdotV, float NdotL)
{
  /*if(fcounter%240 < 120)
	return geomGGX(roughness, NdotV) * geomGGX(roughness, NdotL);
  else*/
	return geomSchlickGGX(roughness, NdotV) * geomSchlickGGX(roughness, NdotL);
  //RETURNS NAN SOMEHOW
}

vec3 brdf(vec3 F, float roughness, vec3 half_dir, vec3 N, float NdotL, float NdotV)
{
	float D = distribGGX(N, half_dir, roughness);
	float G = geomSmith(roughness, NdotV, NdotL);

	float denom = 4 * NdotV * NdotL + 0.001;
	//denom = max(denom, 0.001); // Keep denominator non-null

	return D*F*G / denom;
}

vec4 pbr()
{
	vec4 albedo = texture(coltex, tex_coord);
	if((albedo.a == 0) != ALPHA_PASS)
	{
		discard;
		return vec4(0);
	}
	vec3 pos = texture(postex, tex_coord).xyz;
	vec3 N = texture(normtex, tex_coord).xyz;
	vec3 mro = texture(mrotex, tex_coord).rgb;
	float ao = mro.r;
	float metallic = mro.b;
	float roughness = mro.g;
	vec3 emiss = texture(emtex, tex_coord).rgb;

	vec3 V = normalize(camera - pos);
	float NdotV = max(dot(N, V), 0);

	// F0 surface reflection color (and intensity) at zero incidence
	vec3 F0 = vec3(0.04);				// Non metallic materials look good with this
	F0 = mix(F0, albedo.rgb, metallic);	// Metallic ones are simply mapped between this and their albedo

  vec3 diffuse;

#ifdef IBL
	vec3 irradiance = texture(irrmap, N).rgb;
	// irradiance seems uniform, maybe this map generation is not correct
	diffuse = irradiance * albedo.rgb;
	// which makes diffuse pretty much similiar to albedo
  diffuse *= albedo.rgb;

	vec3 R = reflect(-V, N);
	vec3 prefiltered = textureLod(envmap, R, roughness * MAX_REFLECTION_LOD).rgb;
	vec2 envBRDF = texture(brdf_int, vec2(NdotV, roughness)).rg;

	vec3 F = fresnelSchlickRough(NdotV, F0, roughness);
	vec3 specular = prefiltered * (F*envBRDF.x + envBRDF.y);
	//specular *= 0.3;

	vec3 kD = (1 - F) * (1 - metallic);
	vec3 ambient = (kD*diffuse + specular) * ao;
#else
	vec3 F; vec3 kD; vec3 specular;
	vec3 ambient = 0.03 * albedo.rgb * ao;
#endif

	vec3 Lo = vec3(0);

	/*for(uint i=0; i<lights.len(); i++)
	{
		Light light = lights[i];*/
	for(int i=0; i<2; i++)
	{
		vec3 lpos = light.pos;
		if(i==1)
		{lpos.y += 0.8; lpos.x *= 0.4;}
		float light_int = plight_int(light, pos);
		//vec3 light_dir = normalize(light.pos - pos);
		vec3 light_dir = normalize(lpos - pos);

		float bias = max(0.0005 * (1.0-dot(N, light_dir)), 0.000005);
		//float bias = max(0.05 * (1.0-dot(N, light_dir)), 0.0005);
		//if(albedo.a >= 0.1) // Don't cast shadow on transparent objects for now
			light_int *= shadow(pos, bias);
		if(light_int == 0)
			//return OEXT34(ambient+emiss);
			continue;

		vec3 radiance = light.diffuse * light_int;
		vec3 half_dir = normalize(light_dir + V);
		float NdotL = max(dot(N, light_dir), 0);

		F = fresnelSchlick(half_dir, V, F0);
		kD = (1 - F) * (1 - metallic);
    diffuse = kD*albedo.rgb;

		specular = brdf(F, roughness, half_dir, N, NdotL, NdotV);

		Lo += (specular + diffuse/PI) * radiance * NdotL;
	}

	return vec4(ambient + Lo + emiss, albedo.a);
}
#endif

void main()
{
#ifdef PHONG
	vec3 pos = texture(postex, tex_coord).xyz;
	vec4 color = phong(pos, plight_int(light, pos));
#endif

#ifdef PBR
  vec4 color = pbr();
#endif

#ifdef SOBEL
    float sobel = sobel(normtex, tex_coord);
    if(sobel>0.5) color.rgb = vec3(1,0,0);
#endif

    main_out = color;

#ifdef PBR
	float emiss = length(texture(emtex, tex_coord).rgb);
	if(emiss>0 || dot(color.rgb, vec3(0.2126,0.7152,0.0722)) > 0.7)
#else
	if(dot(color.rgb, vec3(0.2126,0.7152,0.0722)) > 0.7)
#endif
		hdr = color;
	else hdr = vec4(0);
}
