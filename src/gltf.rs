use cgmath::{prelude::SquareMatrix, Matrix4};
use glium::index::PrimitiveType;
use glium::texture::srgb_texture2d::SrgbTexture2d;
use glium::uniforms::Uniforms;
use glium::{Display, DrawError, DrawParameters, IndexBuffer, Program, Surface, Texture2d, VertexBuffer};
use std::path::Path;

//mod helper;
use crate::helper::{load_srgbtexture, load_texture, AlphaMode, PbrMaterial, VertexNT};

extern crate gltf;

#[derive(Debug)]
struct Node
{
    pub children: Vec<Node>,
    range: (usize, usize),
    material: usize,
    texture: usize,
    nmap: usize,
    mrmap: usize,
    aomap: usize,
    emissmap: usize,
}

impl Node
{
    fn draw<S, U>(
        &self,
        frame: &mut S,
        prog: &Program,
        uniforms: &U,
        params: &DrawParameters,
        colorpass: bool,
        alphapass: bool,
        parent: &NodeHierarchy,
    ) -> Result<(), DrawError>
    where
        S: Surface,
        U: Uniforms,
    {
        if self.range.1 != 0
        {
            let material = parent.get_material(self.material);
            let indices = parent.ibuff.slice(self.range.0..self.range.1).unwrap();
            if !colorpass && !alphapass
            // Depth pass (shadows)
            { frame.draw(&parent.vbuff, indices, prog, uniforms, params)? }
            else
            {
                //if alphapass == match material.alpha_mode{AlphaMode::Blend=>true,_=>false}
                if match material.alpha_mode
                {
                    AlphaMode::Blend => true,
                    _ => false,
                } || !alphapass
                {
                    let tex = parent.get_srgbtex(self.texture);
                    let nmap = parent.get_tex(self.nmap);
                    let mrmap = parent.get_tex(self.mrmap);
                    let aomap = parent.get_tex(self.aomap);
                    let emmap = parent.get_srgbtex(self.emissmap);
                    let uniforms = puniform!(
                        material,
                        uniform!(textured: self.texture!=0, ntextured: self.nmap!=0, mrtextured: self.mrmap!=0, aotextured: self.aomap!=0, emtextured: self.emissmap!=0,
                    albedotex:tex, nmap:nmap, mrmap:mrmap, aomap:aomap, emmap:emmap,alphapass:alphapass)
                    ).add_ref(uniforms);
                    frame.draw(&parent.vbuff, indices, prog, &uniforms, params)?
                }
            }
        }

        for child in &self.children
        { child.draw(frame, prog, uniforms, params, colorpass, alphapass, parent)? }

        Ok(())
    }
}

#[derive(Debug)]
pub struct NodeHierarchy
{
    nodes: Vec<Node>,
    vbuff: VertexBuffer<VertexNT>,
    ibuff: IndexBuffer<u32>,
    textures: Vec<(usize, Texture2d)>,
    srgb_textures: Vec<(usize, SrgbTexture2d)>,
    materials: Vec<PbrMaterial>,
    loaded_texture: usize,
    loaded_material: usize,
}

impl NodeHierarchy
{
    pub fn has_transparency(&self) -> bool
    {
        self.materials.iter().any(|m| m.alpha_mode == AlphaMode::Blend)
    }

    pub fn draw<S, U>(
        &self,
        frame: &mut S,
        prog: &Program,
        uniforms: &U,
        params: &DrawParameters,
        colorpass: bool,
        alphapass: bool,
    ) -> Result<(), DrawError>
    where
        S: Surface,
        U: Uniforms,
    {
        for node in &self.nodes
        { node.draw(frame, prog, uniforms, params, colorpass, alphapass, self)? }
        Ok(())
    }

    fn get_tex(&self, index: usize) -> &Texture2d
    {
        &self.textures.iter().find(|(idx, _)| *idx == index).unwrap().1
        /*if let Some((_, tex)) = self.textures.iter().find(|(idx, _)| *idx == index)
        { tex } else { &self.textures[0].1 }*/
    }

    fn get_srgbtex(&self, index: usize) -> &SrgbTexture2d
    {
        &self.srgb_textures.iter().find(|(idx, _)| *idx == index).unwrap().1
        /*if let Some((_, tex)) = self.srgb_textures.iter().find(|(idx, _)| *idx == index)
        { tex } else { &self.srgb_textures[0].1 }*/
    }

    fn get_material(&self, index: usize) -> &PbrMaterial
    {
        &self.materials[index]
    }
}

fn load_gltf_texture<'a, T>(
    disp: &Display,
    dir: &Path,
    tex: Option<T>,
    store: &mut Vec<(usize, Texture2d)>,
) where T: AsRef<gltf::Texture<'a>>,
{
    if tex.is_none() { return }
    let tex = tex.unwrap();
    let tex = tex.as_ref();

    let idx = tex.index() + 1;

    if let gltf::image::Source::Uri { uri, .. } = tex.source().source()
    {
        let path = dir.join(Path::new(uri));
        let path = path.to_str().unwrap();
        store.push((idx, load_texture(disp, path).unwrap()));
    }
}

fn load_gltf_srgbtexture<'a, T>(
    disp: &Display,
    dir: &Path,
    tex: Option<T>,
    store: &mut Vec<(usize, SrgbTexture2d)>,
) where T: AsRef<gltf::Texture<'a>>,
{
    if tex.is_none() { return }
    let tex = tex.unwrap();
    let tex = tex.as_ref();

    let idx = tex.index() + 1;

    if let gltf::image::Source::Uri { uri, .. } = tex.source().source()
    {
        let path = dir.join(Path::new(uri));
        let path = path.to_str().unwrap();
        store.push((idx, load_srgbtexture(disp, path).unwrap()));
    }
}

fn load_gltf_node(
    disp: &Display,
    dir: &Path,
    node: gltf::Node,
    buffers: &Vec<gltf::buffer::Data>,
    transform: Matrix4<f32>,
    vertices: &mut Vec<VertexNT>,
    indices: &mut Vec<u32>,
) -> Node
{
    let transform = transform * Matrix4::from(node.transform().matrix());

    let start = indices.len();
    let vstart = vertices.len() as u32;
    let mut node_tex = 0usize;
    let mut node_nmap = 0usize;
    let mut node_mrmap = 0usize;
    let mut node_aomap = 0usize;
    let mut node_emmap = 0usize;
    let mut node_mat = 0usize;

    if let Some(mesh) = node.mesh()
    {
        for prim in mesh.primitives()
        {
            let material = prim.material();
            if let Some(idx) = material.index()
            { node_mat = idx + 1 }
            let pbr = material.pbr_metallic_roughness();

            if let Some(tex) = pbr.base_color_texture()
            { node_tex = tex.texture().index() + 1 }
            if let Some(tex) = material.normal_texture()
            { node_nmap = tex.texture().index() + 1 }
            if let Some(tex) = pbr.metallic_roughness_texture()
            { node_mrmap = tex.texture().index() + 1 }
            if let Some(tex) = material.occlusion_texture()
            { node_aomap = tex.texture().index() + 1 }
            if let Some(tex) = material.emissive_texture()
            { node_emmap = tex.texture().index() + 1 }

            let reader = prim.reader(|buff| Some(&buffers[buff.index()]));
            //vertices.append(&mut reader.read_positions().unwrap().map(|pos| Vertex::from(pos)).collect());
            indices.append
            (
                &mut reader
                    .read_indices().unwrap()
                    .into_u32()
                    .map(|i| i + vstart)
                    .collect(),
            );
            let mut positions = reader.read_positions().unwrap();
            let mut normals = reader.read_normals().unwrap();
            let tex_coords = reader.read_tex_coords(0);
            if tex_coords.is_some()
            {
                let mut tex_coords = tex_coords.unwrap().into_f32();
                let iterator = positions.zip(normals.zip(tex_coords));
                for (pos, (norm, tex)) in iterator
                {
                    vertices.push(transform *
                            VertexNT
                            {
                                pos: (pos[0], pos[1], pos[2]),
                                norm: (norm[0], norm[1], norm[2]),
                                tex: (tex[0], 1.0 - tex[1]),
                            },
                    );
                }
            }
            else
            {
                let iterator = positions.zip(normals);
                for (pos, norm) in iterator
                {
                    vertices.push(transform *
                        VertexNT
                        {
                            pos: (pos[0], pos[1], pos[2]),
                            norm: (norm[0], norm[1], norm[2]),
                            tex: (0.0, 0.0),
                        },
                    );
                }
            }
        }
    }
    let end = indices.len();

    Node
    {
        range: if start == end { (0, 0) } else { (start, end) },
        children: node.children()
            .map(|child| load_gltf_node(disp, dir, child, buffers, transform, vertices, indices))
            .collect(),
        material: node_mat,
        texture: node_tex,
        nmap: node_nmap,
        mrmap: node_mrmap,
        aomap: node_aomap,
        emissmap: node_emmap,
    }
}

impl<'a> From<gltf::Material<'a>> for crate::helper::PbrMaterial
{
    fn from(mat: gltf::Material) -> Self
    {
        let colorFactor = mat.pbr_metallic_roughness().base_color_factor();
        println!("{:#?}", mat.alpha_mode());
        Self
        {
            albedoFactor: colorFactor,
            /*[
                colorFactor[0],
                colorFactor[1],
                colorFactor[2],
                colorFactor[3],
            ],*/
            roughnessFactor: mat.pbr_metallic_roughness().roughness_factor(),
            metallicFactor: mat.pbr_metallic_roughness().metallic_factor(),
            emissiveFactor: mat.emissive_factor(),
            occlusionFactor: if let Some(tex) = mat.occlusion_texture()
            {tex.strength()} else { 1.0 },
            alpha_mode: mat.alpha_mode().into(),
            ..Default::default()
        }
    }
}

impl From<gltf::material::AlphaMode> for AlphaMode
{
    fn from(mode: gltf::material::AlphaMode) -> Self
    {
        use self::AlphaMode::*;
        match mode
        {
            gltf::material::AlphaMode::Opaque => Opaque,
            gltf::material::AlphaMode::Mask => Mask,
            gltf::material::AlphaMode::Blend => Blend,
        }
    }
}

pub fn load_gltf(
    disp: &Display,
    path: &str,
    //scene_idx: usize,
    transform: Matrix4<f32>,
) -> NodeHierarchy
{
    let (gltf, buffers, images) = gltf::import(path).unwrap();
    if let Some(anim) = gltf.animations().next()
    {
        for chan in anim.channels()
        {
            let target = chan.target();
            let node = target.node().index();
            let property = target.property();
            println!("{:?}", (node, property));
            let sampler = chan.sampler();
            let interp = sampler.interpolation();
            let inp = sampler.input().view().unwrap().length();
            let outp = sampler.output().view().unwrap().length();
            println!("{:?}", (interp, inp, outp));
            break;
        }
    }
    let scene = match gltf.default_scene()
    {
        Some(scene) => scene,
        None => gltf.scenes().next().unwrap()
    };
    //let scene = gltf.scenes().nth(scene_idx);
    let dir = Path::new(path).parent().unwrap();

    let mut vertices = vec![];
    let mut indices = vec![];
    let mut textures = vec![(0, Texture2d::new(disp, vec!(vec!((255u8, 255u8, 255u8)))).unwrap())];
    let mut srgbtex = vec![(0, SrgbTexture2d::new(disp, vec!(vec!((255u8, 255u8, 255u8)))).unwrap())];
    /*let mut textures = vec![(0, Texture2d::empty(disp, 0, 0).unwrap())];
    let mut srgbtex = vec![(0, SrgbTexture2d::empty(disp, 0, 0).unwrap())];*/
    let mut materials = vec![PbrMaterial::default()];

    for mat in gltf.materials()
    {
        load_gltf_texture(disp, dir, mat.normal_texture(), &mut textures);
        load_gltf_texture(disp, dir, mat.occlusion_texture(), &mut textures);
        load_gltf_srgbtexture(disp, dir, mat.emissive_texture(), &mut srgbtex);
        let pbr = mat.pbr_metallic_roughness();
        load_gltf_srgbtexture(disp, dir, pbr.base_color_texture(), &mut srgbtex);
        load_gltf_texture(disp, dir, pbr.metallic_roughness_texture(), &mut textures);
        materials.push(mat.into());
    }

    NodeHierarchy
    {
        nodes: scene.nodes().map(|node|
        {
            load_gltf_node(disp, dir, node, &buffers, transform, &mut vertices, &mut indices)
        }).collect(),
        vbuff: VertexBuffer::new(disp, &vertices[..]).unwrap(),
        ibuff: IndexBuffer::new(disp, PrimitiveType::TrianglesList, &indices[..]).unwrap(),
        textures,
        srgb_textures: srgbtex,
        materials,
        loaded_texture: usize::max_value(),
        loaded_material: usize::max_value(),
    }
}
