use std::time::Instant;

use glium::{BlitTarget, Display, DrawError, DrawParameters, Program, Surface, VertexBuffer};
//use glium::backend::Facade;
use cgmath::{Matrix, Matrix4, Point3, SquareMatrix, Vector4};
use glium::framebuffer::SimpleFrameBuffer;
use glium::index::{IndexBuffer, NoIndices, PrimitiveType};
use glium::program::ProgramCreationError;
use glium::texture::srgb_cubemap::SrgbCubemap;
use glium::texture::{
    CubeLayer, MipmapsOption, SrgbTexture2d, Texture2d, TextureCreationError,
    UncompressedFloatFormat, SrgbFormat,
};
use glium::uniforms::{
    AsUniformValue, EmptyUniforms, MagnifySamplerFilter, MinifySamplerFilter, SamplerWrapFunction,
    UniformValue, Uniforms,
};
use std::ops::Mul;

pub struct UniformsRefStoragePair<'a, R, S>
(R, &'a S)
where
    R: Uniforms,
    S: Uniforms,
;

impl<'a, R, S> Uniforms for UniformsRefStoragePair<'a, R, S>
where
    R: Uniforms,
    S: Uniforms,
{
    fn visit_values<'b, F: FnMut(&str, UniformValue<'b>)>(&'b self, mut output: F) {
        self.0.visit_values(|n, v| output(n, v));
        self.1.visit_values(|n, v| output(n, v));
    }
}

pub struct UniformStoragePair<R, S>
(R, S)
where
    R: Uniforms,
    S: Uniforms,
;

impl<R> UniformStoragePair<R, EmptyUniforms>
where
    R: Uniforms,
{
    pub fn new(v0: R) -> Self
    {
        Self(v0, EmptyUniforms)
    }
}

impl<R, S> UniformStoragePair<R, S>
where
    R: Uniforms,
    S: Uniforms,
{

    pub fn add<T>(self, v: T) -> UniformStoragePair<T, Self>
    where T: Uniforms
    {
        UniformStoragePair(v, self)
    }

    pub fn add_ref<'b, T>(self, v: &'b T) -> UniformsRefStoragePair<'b, Self, T>
        where T: Uniforms
    {
        UniformsRefStoragePair(self, v)
    }
}

impl<R, S> Uniforms for UniformStoragePair<R, S>
where
    R: Uniforms,
    S: Uniforms,
{
    fn visit_values<'b, F: FnMut(&str, UniformValue<'b>)>(&'b self, mut output: F) {
        self.0.visit_values(|n, v| output(n, v));
        self.1.visit_values(|n, v| output(n, v));
    }
}

#[macro_export]
macro_rules! puniform
{
    //($n:ident) => { uniform!(stringify!($n): $n) };
    ($u0:expr, $($u:expr),*) =>
    {
        $crate::helper::UniformStoragePair::new($u0)
        $(.add($u))*
    };

    /*($u1:expr, $u2:expr) => {::helper::UniformStoragePair{store:($u1,$u2)}};
    ($u1:expr, $($name:ident: $value:expr),+) =>
    {puniform!($u1, &uniform!($($name:$value)+,))}*/
}

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub pos: (f32, f32, f32),
}

impl From<[f32; 3]> for Vertex {
    fn from(p: [f32; 3]) -> Self {
        Self {
            pos: (p[0], p[1], p[2]),
        }
    }
}

impl From<Point3<f32>> for Vertex {
    fn from(p: Point3<f32>) -> Self {
        Self {
            pos: (p.x, p.y, p.z),
        }
    }
}

#[derive(Debug, Copy, Clone, Default)]
pub struct VertexNT {
    pub pos: (f32, f32, f32),
    pub norm: (f32, f32, f32),
    pub tex: (f32, f32),
}

impl VertexNT {
    /*fn transform(&self, matrix: Matrix4<f32>) -> Self
    {
        matrix * *self
    }*/

    /*fn transform_self(&mut self, matrix: Matrix4)
    {
        self = self.trans
        self.pos = matrix.transform_vector(self.pos.into()).into();
        self.norm = matrix.invert().unwrap().transpose().transform_vector(self.norm.into()).into();
    }*/
}

impl Mul<VertexNT> for Matrix4<f32> {
    type Output = VertexNT;

    fn mul(self, vertex: VertexNT) -> VertexNT {
        let inverse = self.invert();
        VertexNT {
            pos: (self * Vector4::new(vertex.pos.0, vertex.pos.1, vertex.pos.2, 1.0))
                .truncate()
                .into(),
            //norm: self.invert().unwrap().transpose().transform_vector(vertex.norm.into()).into(),
            //TODO: Find why transform matrix can be non-invertible (endless_floor triggers a crash
            //because of that)
            norm:
                (if let Some(inverse)=inverse{inverse.transpose()}else{Self::identity()}                  // Possibly give up on
                   * Vector4::new(vertex.norm.0, vertex.norm.1, vertex.norm.2, 1.0))
                .truncate()
                .into(), // normals transform
            ..vertex
        }
    }
}

/*impl<T> Mul<Matrix4<f32>> for T
    where T: IntoIterator,
          T::Item: VertexNT
{
    fn mul_assign(&mut self, matrix: Matrix4<f32>)
    {
        self.into_iter().map(|vect| vect*matrix)
    }
}*/

#[derive(Copy, Clone, Default)]
pub struct VertexT {
    pub pos: (f32, f32, f32),
    pub tex: (f32, f32),
}

#[derive(Copy, Clone, Default)]
pub struct Normal {
    pub norm: (f32, f32, f32),
}

implement_vertex!(Vertex, pos);
implement_vertex!(VertexT, pos, tex);
implement_vertex!(VertexNT, pos, norm, tex);
implement_vertex!(Normal, norm);

/*#[derive(Copy, Clone)]
pub enum ColorSource<'a>
{
    Texture(&'a Texture2d),
    Color((f32,f32,f32)),
}

impl<'a> AsUniformValue for ColorSource<'a>
{
    fn as_uniform_value(&self) -> UniformValue
    {
        match self
        {
            ColorSource::Texture(tex) => tex.as_uniform_value(),
            ColorSource::Color(tuple) => tuple.as_uniform_value(),
        }

    }
}*/

#[derive(Copy, Clone)]
pub struct Material {
    pub ambient: (f32, f32, f32),
    pub diffuse: (f32, f32, f32),
    pub specular: (f32, f32, f32),
    pub shininess: f32,
}

// Taken from http://devernay.free.fr/cours/opengl/materials.html
#[allow(dead_code)]
pub const EMERALD: Material = Material {
    ambient: (0.0215, 0.1745, 0.0215),
    diffuse: (0.07568, 0.61424, 0.07568),
    specular: (0.633, 0.727811, 0.633),
    shininess: 0.6,
};
#[allow(dead_code)]
pub const JADE: Material = Material {
    ambient: (0.135, 0.2225, 0.1575),
    diffuse: (0.54, 0.89, 0.63),
    specular: (0.316228, 0.316228, 0.316228),
    shininess: 0.1,
};
#[allow(dead_code)]
pub const OBSIDIAN: Material = Material {
    ambient: (0.05375, 0.05, 0.06625),
    diffuse: (0.18275, 0.17, 0.22525),
    specular: (0.332741, 0.328634, 0.346435),
    shininess: 0.3,
};
#[allow(dead_code)]
pub const PEARL: Material = Material {
    ambient: (0.25, 0.20725, 0.20725),
    diffuse: (1.0, 0.829, 0.829),
    specular: (0.296648, 0.296648, 0.296648),
    shininess: 0.088,
};
#[allow(dead_code)]
pub const RUBY: Material = Material {
    ambient: (0.1745, 0.01175, 0.01175),
    diffuse: (0.61424, 0.04136, 0.04136),
    specular: (0.727811, 0.626959, 0.626959),
    shininess: 0.6,
};
#[allow(dead_code)]
pub const TURQUOISE: Material = Material {
    ambient: (0.1, 0.18725, 0.1745),
    diffuse: (0.396, 0.74151, 0.69102),
    specular: (0.297254, 0.30829, 0.306678),
    shininess: 0.1,
};
#[allow(dead_code)]
pub const BRASS: Material = Material {
    ambient: (0.329412, 0.223529, 0.027451),
    diffuse: (0.780392, 0.568627, 0.113725),
    specular: (0.992157, 0.941176, 0.807843),
    shininess: 0.21794872,
};
#[allow(dead_code)]
pub const BRONZE: Material = Material {
    ambient: (0.2125, 0.1275, 0.054),
    diffuse: (0.714, 0.4284, 0.18144),
    specular: (0.393548, 0.271906, 0.166721),
    shininess: 0.2,
};
#[allow(dead_code)]
pub const CHROME: Material = Material {
    ambient: (0.25, 0.25, 0.25),
    diffuse: (0.4, 0.4, 0.4),
    specular: (0.774597, 0.774597, 0.774597),
    shininess: 0.6,
};
#[allow(dead_code)]
pub const COPPER: Material = Material {
    ambient: (0.19125, 0.0735, 0.0225),
    diffuse: (0.7038, 0.27048, 0.0828),
    specular: (0.256777, 0.137622, 0.086014),
    shininess: 0.1,
};
#[allow(dead_code)]
pub const GOLD: Material = Material {
    ambient: (0.24725, 0.1995, 0.0745),
    diffuse: (0.75164, 0.60648, 0.22648),
    specular: (0.628281, 0.555802, 0.366065),
    shininess: 0.4,
};
#[allow(dead_code)]
pub const SILVER: Material = Material {
    ambient: (0.19225, 0.19225, 0.19225),
    diffuse: (0.50754, 0.50754, 0.50754),
    specular: (0.508273, 0.508273, 0.508273),
    shininess: 0.4,
};
#[allow(dead_code)]
pub const BLACK_PLASTIC: Material = Material {
    ambient: (0.0, 0.0, 0.0),
    diffuse: (0.01, 0.01, 0.01),
    specular: (0.50, 0.50, 0.50),
    shininess: 0.25,
};
#[allow(dead_code)]
pub const CYAN_PLASTIC: Material = Material {
    ambient: (0.0, 0.1, 0.06),
    diffuse: (0.0, 0.50980392, 0.50980392),
    specular: (0.50196078, 0.50196078, 0.50196078),
    ..BLACK_PLASTIC
};
#[allow(dead_code)]
pub const GREEN_PLASTIC: Material = Material {
    ambient: (0.0, 0.0, 0.0),
    diffuse: (0.1, 0.35, 0.1),
    specular: (0.45, 0.55, 0.45),
    ..BLACK_PLASTIC
};
#[allow(dead_code)]
pub const RED_PLASTIC: Material = Material {
    ambient: (0.0, 0.0, 0.0),
    diffuse: (0.5, 0.0, 0.0),
    specular: (0.7, 0.6, 0.6),
    ..BLACK_PLASTIC
};
#[allow(dead_code)]
pub const WHITE_PLASTIC: Material = Material {
    ambient: (0.0, 0.0, 0.0),
    diffuse: (0.55, 0.55, 0.55),
    specular: (0.70, 0.70, 0.70),
    ..BLACK_PLASTIC
};
#[allow(dead_code)]
pub const YELLOW_PLASTIC: Material = Material {
    ambient: (0.0, 0.0, 0.0),
    diffuse: (0.5, 0.5, 0.0),
    specular: (0.60, 0.60, 0.50),
    ..BLACK_PLASTIC
};
#[allow(dead_code)]
pub const BLACK_RUBBER: Material = Material {
    ambient: (0.02, 0.02, 0.02),
    diffuse: (0.01, 0.01, 0.01),
    specular: (0.4, 0.4, 0.4),
    shininess: 0.078125,
};
#[allow(dead_code)]
pub const CYAN_RUBBER: Material = Material {
    ambient: (0.0, 0.05, 0.05),
    diffuse: (0.4, 0.5, 0.5),
    specular: (0.04, 0.7, 0.7),
    ..BLACK_RUBBER
};
#[allow(dead_code)]
pub const GREEN_RUBBER: Material = Material {
    ambient: (0.0, 0.05, 0.0),
    diffuse: (0.4, 0.5, 0.4),
    specular: (0.04, 0.7, 0.04),
    ..BLACK_RUBBER
};
#[allow(dead_code)]
pub const RED_RUBBER: Material = Material {
    ambient: (0.05, 0.0, 0.0),
    diffuse: (0.5, 0.4, 0.4),
    specular: (0.7, 0.04, 0.04),
    ..BLACK_RUBBER
};
#[allow(dead_code)]
pub const WHITE_RUBBER: Material = Material {
    ambient: (0.05, 0.05, 0.05),
    diffuse: (0.5, 0.5, 0.5),
    specular: (0.7, 0.7, 0.7),
    ..BLACK_RUBBER
};
#[allow(dead_code)]
pub const YELLOW_RUBBER: Material = Material {
    ambient: (0.05, 0.05, 0.0),
    diffuse: (0.5, 0.5, 0.4),
    specular: (0.7, 0.7, 0.04),
    ..BLACK_RUBBER
};

#[derive(Copy, Clone)]
pub struct Light {
    pub pos: (f32, f32, f32),
    pub ambient: (f32, f32, f32),
    pub diffuse: (f32, f32, f32),
    pub specular: (f32, f32, f32),
    pub attenuation: (f32, f32, f32),
}

impl Uniforms for &Material
{
    fn visit_values<'a, F>(&'a self, mut visitor: F)
    where
        F: FnMut(&str, UniformValue<'a>),
    {
        visitor("material.ambient", self.ambient.as_uniform_value());
        visitor("material.diffuse", self.diffuse.as_uniform_value());
        visitor("material.specular", self.specular.as_uniform_value());
        visitor("material.shininess", self.shininess.as_uniform_value());
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum AlphaMode {
    Opaque,
    Mask,
    Blend,
}

impl AsUniformValue for AlphaMode {
    fn as_uniform_value(&self) -> UniformValue {
        match self {
            AlphaMode::Opaque => 0.as_uniform_value(),
            AlphaMode::Mask => 1.as_uniform_value(),
            AlphaMode::Blend => 2.as_uniform_value(),
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct PbrMaterial {
    pub albedo: [f32; 3],
    pub albedoFactor: [f32; 4],
    pub roughness: f32,
    pub roughnessFactor: f32,
    pub metallic: f32,
    pub metallicFactor: f32,
    pub emissiveColor: [f32; 3],
    pub emissiveFactor: [f32; 3],
    pub occlusionFactor: f32,
    pub alpha_mode: AlphaMode,
}

impl Uniforms for &PbrMaterial {
    fn visit_values<'a, F>(&'a self, mut visitor: F)
    where
        F: FnMut(&str, UniformValue<'a>),
    {
        visitor("material.albedo", self.albedo.as_uniform_value());
        visitor(
            "material.albedoFactor",
            self.albedoFactor.as_uniform_value(),
        );
        visitor("material.roughness", self.roughness.as_uniform_value());
        visitor(
            "material.roughFactor",
            self.roughnessFactor.as_uniform_value(),
        );
        visitor("material.metallic", self.metallic.as_uniform_value());
        visitor(
            "material.metallicFactor",
            self.metallicFactor.as_uniform_value(),
        );
        visitor(
            "material.emissiveColor",
            self.emissiveColor.as_uniform_value(),
        );
        visitor(
            "material.emissiveFactor",
            self.emissiveFactor.as_uniform_value(),
        );
        visitor(
            "material.occlusionFactor",
            self.occlusionFactor.as_uniform_value(),
        );
        visitor("material.alpha_mode", self.alpha_mode.as_uniform_value());
    }
}

impl Default for PbrMaterial {
    fn default() -> Self {
        Self {
            albedo: [1.0, 1.0, 1.0],
            albedoFactor: [1.0, 1.0, 1.0, 1.0],
            roughness: 1.0,
            roughnessFactor: 1.0,
            metallic: 1.0,
            metallicFactor: 1.0,
            emissiveColor: [1.0, 1.0, 1.0],
            emissiveFactor: [1.0, 1.0, 1.0],
            occlusionFactor: 1.0,
            alpha_mode: AlphaMode::Opaque,
        }
    }
}

impl Uniforms for &Light {
    fn visit_values<'a, F>(&'a self, mut visitor: F)
    where
        F: FnMut(&str, UniformValue<'a>),
    {
        visitor("light.pos", self.pos.as_uniform_value());
        visitor("light.ambient", self.ambient.as_uniform_value());
        visitor("light.diffuse", self.diffuse.as_uniform_value());
        visitor("light.attenuation", self.attenuation.as_uniform_value());
    }
}

impl Default for Light {
    fn default() -> Self {
        Self {
            pos: Default::default(),
            ambient: (1.0, 1.0, 1.0),
            diffuse: (1.0, 1.0, 1.0),
            specular: (1.0, 1.0, 1.0),
            attenuation: (1.0, 0.0, 0.0),
        }
    }
}

pub struct Vibo<V, N, I>
where
    V: glium::Vertex,
    N: glium::Vertex,
    I: glium::index::Index,
{
    vertices: glium::VertexBuffer<V>,
    normals: Option<glium::VertexBuffer<N>>,
    indices: Option<glium::index::IndexBuffer<I>>,
}

impl<V, N, I> Vibo<V, N, I>
where
    V: glium::Vertex,
    N: glium::Vertex,
    I: glium::index::Index,
{
    pub fn new(
        disp: &Display,
        vertices: &[V],
        normals: Option<&[N]>,
        indices: Option<&[I]>,
    ) -> Self {
        let normals = if let Some(arr) = normals {
            Some(VertexBuffer::new(disp, arr).unwrap())
        } else {
            None
        };
        let indices = if let Some(arr) = indices {
            Some(IndexBuffer::new(disp, PrimitiveType::TrianglesList, arr).unwrap())
        } else {
            None
        };
        Self {
            vertices: VertexBuffer::new(disp, vertices).unwrap(),
            normals,
            indices,
        }
    }

    pub fn draw<S, U>(
        &self,
        frame: &mut S,
        prog: &Program,
        uniforms: &U,
        params: &DrawParameters,
    ) -> Result<(), DrawError>
    where
        S: Surface,
        U: Uniforms,
    {
        match (&self.normals, &self.indices) {
            (Some(ref normals), Some(ref indices)) => {
                frame.draw((&self.vertices, normals), indices, prog, uniforms, params)
            }
            (Some(ref normals), None) => frame.draw(
                (&self.vertices, normals),
                &NoIndices(PrimitiveType::TrianglesList),
                prog,
                uniforms,
                params,
            ),
            (None, Some(ref indices)) => {
                frame.draw(&self.vertices, indices, prog, uniforms, params)
            }
            (None, None) => frame.draw(
                &self.vertices,
                &NoIndices(PrimitiveType::TrianglesList),
                prog,
                uniforms,
                params,
            ),
        }
    }
}

#[allow(dead_code)]
pub struct Drawable<'a, V, N, I>
where
    V: glium::Vertex + 'a,
    N: glium::Vertex + 'a,
    I: glium::index::Index,
{
    pub transform: Matrix4<f32>,
    pub geom: &'a Vibo<V, N, I>,
}

#[allow(dead_code)]
impl<'a, V, N, I> Drawable<'a, V, N, I>
where
    V: glium::Vertex,
    N: glium::Vertex,
    I: glium::index::Index,
{
    pub fn draw<S, U>(
        &self,
        frame: &mut S,
        prog: &Program,
        uniforms: U,
        params: &DrawParameters,
    ) -> Result<(), DrawError>
    where
        S: Surface,
        U: Uniforms,
    {
        let model: [[f32; 4]; 4] = self.transform.into();
        self.geom.draw(
            frame,
            prog,
            &puniform!(uniforms, uniform!(model: model, model2: model)),
            params,
        )
    }
}

pub fn load_texture(disp: &Display, filepath: &str) -> Result<Texture2d, TextureCreationError> {
    use glium::texture::RawImage2d;

    let mut img = image::open(filepath).unwrap();
    /*if format == image::ImageFormat::TGA {
        img = img.flipv()
    }*/
    /*if &*filepath[filepath.len() - 3..].to_string().to_uppercase() == "TGA"
    { img = img.flipv() }*/
    let img = img.to_rgba();

    let dim = img.dimensions();
    let img = if &*filepath[filepath.len() - 3..].to_string().to_uppercase() == "TGA"
    {RawImage2d::from_raw_rgba(img.into_raw(), dim)}
    else{RawImage2d::from_raw_rgba_reversed(&img.into_raw()[..], dim)};
    //let img = RawImage2d::from_raw_rgba_reversed(&img.into_raw(), dim);
    Texture2d::new(disp, img)
}

pub fn load_srgbtexture(
    disp: &Display,
    filepath: &str,
) -> Result<SrgbTexture2d, TextureCreationError> {
    use glium::texture::RawImage2d;

    println!("Reading texture");
    let mut img = match image::open(filepath)
    {
        Ok(img) => img,
        r @ Err(_) =>
        {
            eprintln!("{}", filepath);
            r.unwrap();
            panic!()
        }
    };

    if &*filepath[filepath.len() - 3..].to_string().to_uppercase() == "TGA" {
        println!("Flipping texture");
        img = img.flipv()
    }
    println!("Converting to RGBA texture");
    let img = img.to_rgba();

    let dim = img.dimensions();
    let img = RawImage2d::from_raw_rgba_reversed(&img.into_raw(), dim);
    SrgbTexture2d::new(disp, img)
}

fn srgb_to_surface<'a>(facade: &Display, tex: &'a SrgbTexture2d) -> SimpleFrameBuffer<'a>
{
    SimpleFrameBuffer::new(facade, tex).unwrap()
}

/**
 * Filepaths should be given in the following order:
 * +X, -X
 * +Y, -Y,
 * +Z, -Z
 **/
pub fn load_cubemap<I>(disp: &Display, filepathes: I) -> SrgbCubemap
where
    I: IntoIterator,
    I::Item: Into<String>,
{
    let textures: Vec<SrgbTexture2d> = filepathes
        .into_iter()
        .map(|path| load_srgbtexture(disp, &path.into()[..]).unwrap())
        .collect();

    let cubemap = SrgbCubemap::empty_with_format(disp, SrgbFormat::U8U8U8, MipmapsOption::NoMipmap, textures[0].width()).unwrap();

    let dest_rect = BlitTarget {
        left: 0,
        bottom: 0,
        width: textures[0].width() as i32,
        height: textures[0].height() as i32,
    };

    println!("Uploading textures");
    use self::CubeLayer::*;
    for (i, layer) in [
        PositiveX, NegativeX, PositiveY, NegativeY, PositiveZ, NegativeZ,
    ]
    .iter()
    .enumerate()
    {
        let fb = SimpleFrameBuffer::new(disp, cubemap.main_level().image(*layer)).unwrap();
        srgb_to_surface(disp, &textures[i])
            .blit_whole_color_to(&fb, &dest_rect, MagnifySamplerFilter::Linear);
    }

    cubemap
}

pub fn convolute_cubemap<V, N, I>(
    disp: &Display,
    cubemap: &SrgbCubemap,
    dimension: u16,
    skybox: &Vibo<V, N, I>,
    init: Option::<Instant>,
) -> SrgbCubemap
where
    V: glium::Vertex,
    N: glium::Vertex,
    I: glium::index::Index,
{
    let cubemap = cubemap.sampled();

    let convolprog = load_prog(
        disp,
        ShaderSource::File("assets/shaders/skybox.vert"),
        ShaderSource::File("assets/shaders/pbr/convolute.frag"),
        None,
        vec![],
    )
    .expect("Error compiling convolution shaders");
    let convoluted = SrgbCubemap::empty(disp, dimension as u32).unwrap();

    use cgmath::{perspective, Deg, Vector3};
    let proj: [[f32; 4]; 4] = perspective(Deg(90f32), 1f32, 0.1f32, 10f32).into();
    let origin = Point3::new(0.0, 0.0, 0f32);
    let identity: [[f32; 4]; 4] = Matrix4::identity().into();
    let views: [[[f32; 4]; 4]; 6] = [
        Matrix4::look_at(origin, Point3::new(1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
        Matrix4::look_at(origin, Point3::new(-1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, 1.0, 0.0), Vector3::unit_z()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, -1.0, 0.0), -Vector3::unit_z()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, 0.0, 1.0), -Vector3::unit_y()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, 0.0, -1.0), -Vector3::unit_y()).into(),
    ];

    let draw_params = DrawParameters::default();

    use self::CubeLayer::*;
    for (i, layer) in [
        PositiveX, NegativeX, PositiveY, NegativeY, PositiveZ, NegativeZ,
    ].iter().enumerate()
    {
        let mut fb = SimpleFrameBuffer::new(disp, convoluted.main_level().image(*layer)).unwrap();
        let view = uniform!(view:views[i]);
        if i == 0
        {
            skybox.draw(
                &mut fb,
                &convolprog,
                &puniform!(view, uniform!(proj: proj, model: identity, cubemap: cubemap)),
                &draw_params,
            ).unwrap()
        }
        else
        {
            skybox.draw(&mut fb, &convolprog, &view, &draw_params).unwrap()
        }
        if let Some(init) = init
        {
            println!("{:?} Convoluted {:?} skybox face", Instant::now() - init, layer);
        }
    }

    convoluted
}

pub fn prefilter_cubemap<V, N, I>(
    disp: &Display,
    cubemap: &SrgbCubemap,
    base_dim: u16,
    skybox: &Vibo<V, N, I>,
) -> SrgbCubemap
where
    V: glium::Vertex,
    N: glium::Vertex,
    I: glium::index::Index,
{
    let mip_count = 5;

    let cubemap = cubemap
        .sampled()
        .wrap_function(SamplerWrapFunction::Clamp)
        .minify_filter(MinifySamplerFilter::LinearMipmapLinear)
        .magnify_filter(MagnifySamplerFilter::Linear);

    let prefprog = load_prog(
        disp,
        ShaderSource::File("assets/shaders/skybox.vert"),
        ShaderSource::File("assets/shaders/pbr/prefilter.frag"),
        None,
        vec![],
    )
    .expect("Error compiling prefiltering shader");
    let prefiltered = SrgbCubemap::empty_with_mipmaps(
        disp,
        MipmapsOption::AutoGeneratedMipmapsMax(mip_count - 1),
        base_dim as u32,
    ).unwrap();
    // We use mip_count-1 because base level is not counted in it

    use cgmath::{perspective, Deg, Vector3};
    let proj: [[f32; 4]; 4] = perspective(Deg(90f32), 1f32, 0.1f32, 10f32).into();
    let origin = Point3::new(0.0, 0.0, 0f32);
    let identity: [[f32; 4]; 4] = Matrix4::identity().into();
    let views: [[[f32; 4]; 4]; 6] = [
        Matrix4::look_at(origin, Point3::new(1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
        Matrix4::look_at(origin, Point3::new(-1.0, 0.0, 0.0), -Vector3::unit_y()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, 1.0, 0.0), Vector3::unit_z()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, -1.0, 0.0), -Vector3::unit_z()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, 0.0, 1.0), -Vector3::unit_y()).into(),
        Matrix4::look_at(origin, Point3::new(0.0, 0.0, -1.0), -Vector3::unit_y()).into(),
    ];

    let draw_params = DrawParameters::default();

    for mip in 0..mip_count {
        let level = prefiltered.mipmap(mip).expect(&format!(
            "Error getting mipmap {} while prefiltering cubemap",
            mip
        ));
        let roughness = mip as f32 / (mip_count - 1) as f32;

        use self::CubeLayer::*;
        for (i, layer) in [
            PositiveX, NegativeX, PositiveY, NegativeY, PositiveZ, NegativeZ,
        ]
        .iter()
        .enumerate()
        {
            let mut fb = SimpleFrameBuffer::new(disp, level.image(*layer)).unwrap();
            match (mip, i)
            {
                // First draw, must send MVP
                (0, 0) =>
                {
                    let uniforms = uniform!(proj:proj,model:identity,cubemap:cubemap, view:views[i], roughness:roughness);
                    skybox.draw(&mut fb, &prefprog, &uniforms, &draw_params).unwrap()
                }
                // First layer, roughness (and mip) changed
                (_, 0) => skybox.draw(
                        &mut fb,
                        &prefprog,
                        &uniform!(view:views[i], roughness:roughness),
                        &draw_params,
                    ).unwrap(),
                _ => skybox.draw(&mut fb, &prefprog, &uniform!(view:views[i]), &draw_params).unwrap(),
            }
        }
    }

    prefiltered
}

pub fn integrate_brdf<V, N, I>(disp: &Display, dimension: u16, square: &Vibo<V, N, I>) -> Texture2d
where
    V: glium::Vertex,
    N: glium::Vertex,
    I: glium::index::Index,
{
    let result = Texture2d::empty_with_format(
        disp,
        UncompressedFloatFormat::F16F16,
        MipmapsOption::NoMipmap,
        dimension as u32,
        dimension as u32,
    ).unwrap();

    let integrate_prog = load_prog(
        disp,
        ShaderSource::File("assets/shaders/identity.vert"),
        ShaderSource::File("assets/shaders/pbr/brdf_int.frag"),
        None,
        vec![],
    )
    .expect("Error compiling BRDF integration shader");
    square.draw(
            &mut result.as_surface(),
            &integrate_prog,
            &EmptyUniforms,
            &DrawParameters::default(),
        ).unwrap();

    result
}

/*pub fn prog_from_source<'a, F: ?Sized>(facade: &F, vertex_shader: &'a str, fragment_shader: &'a str, geometry_shader: Option<&'a str>, outputs_srgb: bool) -> Result<Program, ProgramCreationError>
    where F: Facade
{
    Program::new(facade, ProgramCreationInput::SourceCode {
        vertex_shader: vertex_shader,
        fragment_shader: fragment_shader,
        geometry_shader: geometry_shader,
        tessellation_control_shader: None,
        tessellation_evaluation_shader: None,
        transform_feedback_varyings: None,
        outputs_srgb: outputs_srgb,
        uses_point_size: false,
    })
}*/

fn load_file<'a>(path: &'a str) -> String {
    use std::{fs::File, io::prelude::*};

    let mut content = String::new();
    File::open(path)
        .expect(&format!("Failed to open {}", path)[..])
        .read_to_string(&mut content).unwrap();
    content
}

#[derive(PartialEq)]
pub enum ShaderSource<'a> {
    File(&'a str),
    Src(&'a str),
    Empty,
}

pub fn load_prog<'a>(
    disp: &Display,
    vshad: ShaderSource<'a>,
    fshad: ShaderSource<'a>,
    gshad: Option<ShaderSource<'a>>,
    mut defines: Vec<&'a str>,
) -> Result<Program, ProgramCreationError> {
    use self::ShaderSource::*;

    let mut vshad = match vshad {
        File(path) => load_file(path),
        Src(src) => src.into(),
        Empty => "#version 330\nvoid main(){}".into(),
    };
    let mut fshad = match fshad {
        File(path) => load_file(path),
        Src(src) => src.into(),
        Empty => "#version 330\nvoid main(){}".into(),
    };
    let mut gshad = match gshad {
        Some(File(path)) => Some(load_file(path)),
        Some(Src(src)) => Some(src.into()),
        Some(Empty) => Some("#version 330\nvoid main(){}".into()),
        None => None,
    };
    if gshad.is_some() {
        defines.push("USE_GEOM_SHAD");
    }
    for def in defines {
        let def = format!("\n#define {}\n", def);
        vshad = vshad.replacen("\n", &def[..], 1);
        fshad = fshad.replacen("\n", &def[..], 1);
        if let Some(src) = gshad {
            gshad = Some(src.replacen("\n", &def[..], 1))
        }
    }
    // Seems to requires #version 150
    // No effects on OpenGL, only openGL ES
    //vshad = vshad.replacen("\n", "\nprecision lowp float;\n", 1);
    //fshad = fshad.replacen("\n", "\nprecision lowp float;\n", 1);

    if let Some(gshad) = gshad {
        Program::from_source(disp, &vshad[..], &fshad[..], Some(&gshad[..]))
    } else {
        Program::from_source(disp, &vshad[..], &fshad[..], None)
    }
}

#[allow(dead_code)]
pub const IDENTITY_4: [[f32; 4]; 4] = [
    [1.0, 0.0, 0.0, 0.0],
    [0.0, 1.0, 0.0, 0.0],
    [0.0, 0.0, 1.0, 0.0],
    [0.0, 0.0, 0.0, 1.0],
];

#[allow(dead_code)]
pub const CUBE_V: [VertexNT; 8] = [
    VertexNT {
        pos: (-1.0, 1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 1.0),
    }, // 0
    VertexNT {
        pos: (1.0, 1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 1.0),
    }, // 1
    VertexNT {
        pos: (1.0, -1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 0.0),
    }, // 2
    VertexNT {
        pos: (-1.0, -1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 0.0),
    }, // 3
    VertexNT {
        pos: (-1.0, 1.0, -1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 1.0),
    }, // 4
    VertexNT {
        pos: (1.0, 1.0, -1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 1.0),
    }, // 5
    VertexNT {
        pos: (1.0, -1.0, -1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 0.0),
    }, // 6
    VertexNT {
        pos: (-1.0, -1.0, -1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 0.0),
    }, // 7
];

#[allow(dead_code)]
pub const CUBE_I: [u8; 36] = [
    3, 1, 0, 3, 2, 1, 2, 5, 1, 2, 6, 5, 6, 4, 5, 6, 7, 4, 7, 0, 4, 7, 3, 0, 0, 5, 4, 0, 1, 5, 7, 2,
    3, 7, 6, 2,
];

pub const CUBEN_V: [VertexNT; 24] = [
    VertexNT {
        pos: (-1.0, 1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 1.0),
    },
    VertexNT {
        pos: (1.0, 1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 1.0),
    },
    VertexNT {
        pos: (1.0, -1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, -1.0, 1.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 0.0),
    },
    VertexNT {
        pos: (1.0, 1.0, 1.0),
        norm: (1.0, 0.0, 0.0),
        tex: (0.0, 1.0),
    },
    VertexNT {
        pos: (1.0, 1.0, -1.0),
        norm: (1.0, 0.0, 0.0),
        tex: (1.0, 1.0),
    },
    VertexNT {
        pos: (1.0, -1.0, -1.0),
        norm: (1.0, 0.0, 0.0),
        tex: (1.0, 0.0),
    },
    VertexNT {
        pos: (1.0, -1.0, 1.0),
        norm: (1.0, 0.0, 0.0),
        tex: (0.0, 0.0),
    },
    VertexNT {
        pos: (1.0, 1.0, -1.0),
        norm: (0.0, 0.0, -1.0),
        tex: (1.0, 1.0),
    },
    VertexNT {
        pos: (-1.0, 1.0, -1.0),
        norm: (0.0, 0.0, -1.0),
        tex: (0.0, 1.0),
    },
    VertexNT {
        pos: (-1.0, -1.0, -1.0),
        norm: (0.0, 0.0, -1.0),
        tex: (0.0, 0.0),
    },
    VertexNT {
        pos: (1.0, -1.0, -1.0),
        norm: (0.0, 0.0, -1.0),
        tex: (1.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, 1.0, -1.0),
        norm: (-1.0, 0.0, 0.0),
        tex: (0.0, 1.0),
    },
    VertexNT {
        pos: (-1.0, 1.0, 1.0),
        norm: (-1.0, 0.0, 0.0),
        tex: (1.0, 1.0),
    },
    VertexNT {
        pos: (-1.0, -1.0, 1.0),
        norm: (-1.0, 0.0, 0.0),
        tex: (1.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, -1.0, -1.0),
        norm: (-1.0, 0.0, 0.0),
        tex: (0.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, 1.0, -1.0),
        norm: (0.0, 1.0, 0.0),
        tex: (0.0, 1.0),
    },
    VertexNT {
        pos: (1.0, 1.0, -1.0),
        norm: (0.0, 1.0, 0.0),
        tex: (1.0, 1.0),
    },
    VertexNT {
        pos: (1.0, 1.0, 1.0),
        norm: (0.0, 1.0, 0.0),
        tex: (1.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, 1.0, 1.0),
        norm: (0.0, 1.0, 0.0),
        tex: (0.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, -1.0, 1.0),
        norm: (0.0, -1.0, 0.0),
        tex: (0.0, 1.0),
    },
    VertexNT {
        pos: (1.0, -1.0, 1.0),
        norm: (0.0, -1.0, 0.0),
        tex: (1.0, 1.0),
    },
    VertexNT {
        pos: (1.0, -1.0, -1.0),
        norm: (0.0, -1.0, 0.0),
        tex: (1.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, -1.0, -1.0),
        norm: (0.0, -1.0, 0.0),
        tex: (0.0, 0.0),
    },
];

pub const CUBEN_I: [u8; 36] = [
    3, 1, 0, 3, 2, 1, 7, 5, 4, 7, 6, 5, 11, 9, 8, 11, 10, 9, 15, 13, 12, 15, 14, 13, 19, 17, 16,
    19, 18, 17, 23, 21, 20, 23, 22, 21,
];

pub const SQUARE_V: [VertexNT; 4] = [
    VertexNT {
        pos: (-1.0, 1.0, 0.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 1.0),
    },
    VertexNT {
        pos: (1.0, 1.0, 0.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 1.0),
    },
    VertexNT {
        pos: (1.0, -1.0, 0.0),
        norm: (0.0, 0.0, 1.0),
        tex: (1.0, 0.0),
    },
    VertexNT {
        pos: (-1.0, -1.0, 0.0),
        norm: (0.0, 0.0, 1.0),
        tex: (0.0, 0.0),
    },
];

pub const SQUARE_I: [u8; 6] = [3, 1, 0, 3, 2, 1];
