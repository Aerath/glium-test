use std::time::{Duration,Instant};

use cgmath::{perspective,ortho,Deg,Matrix4,Vector3,Point3,SquareMatrix,Rad};

#[macro_use]
extern crate glium;
use glium::{glutin,Display,Surface,DrawParameters,Depth, Version, Api};
use glium::draw_parameters::{DepthTest,BackfaceCullingMode,Blend};
use glium::framebuffer::{SimpleFrameBuffer,MultiOutputFrameBuffer,DepthRenderBuffer};
use glium::texture::{MipmapsOption, UncompressedFloatFormat, DepthFormat};
use glium::texture::texture2d::Texture2d;
use glium::texture::depth_texture2d::DepthTexture2d;
use glium::uniforms::{SamplerWrapFunction,MinifySamplerFilter,MagnifySamplerFilter};
use glutin::{WindowBuilder,ContextBuilder,EventsLoop,Event,GlProfile,VirtualKeyCode,ElementState};
use glutin::WindowEvent::*;
use glutin::DeviceEvent::*;
use glutin::dpi::LogicalSize;


#[macro_use]
mod helper;
use crate::helper::{load_prog,Vibo,Light,VertexNT, ShaderSource,load_cubemap, convolute_cubemap, prefilter_cubemap, integrate_brdf};
mod gltf;
use crate::gltf::load_gltf;

const FRAMERATE: u32 = 75+3;
const WDIMS: (u32,u32) = (1920,1080);

const CAM_SLOW:f32 = 0.5;
const CAM_NORM:f32 = 1.0;
const CAM_FAST:f32 = 2.0;

fn main()
{
    let init = Instant::now();

    let mut model = "assets/models/damagedHelmet/damagedHelmet.gltf";
    let mut scale = 1f32;
    let mut fov = 45f32;
    let mut exposure = 1f32;
    let mut sky = String::from("assets/skyboxes/spiaggia_di_mondello_8k/4k_e4/reversed/");
    let mut lag_mode = false;
    let mut demo_mode = false;
    let mut alpha_enabled = true;
    let mut blur_enabled = true;
    let mut shadows_enabled = true;
    let mut hdr_enabled = true;
    let mut supersampling = 2;
    let mut blur_passes = 3;
    let mut edge_filter = false;
    let args: Vec<String> = std::env::args().skip(1).collect();
    let mut args = args.iter();
    while let Some(arg) = args.next()
    {
        match &arg[..]
        {
            "-d"|"--demo" => demo_mode = true,
            "-e"|"--exposure" => exposure = args.next().unwrap().parse().unwrap(),
            "-m"|"--model" => model = args.next().unwrap(),
            "-s"|"--scale" => scale = args.next().unwrap().parse().unwrap(),
            "-S"|"--sky" => sky = args.next().unwrap().clone(),
            "-f"|"--fov" => fov = args.next().unwrap().parse().unwrap(),
            "-l"|"--lag-mode" => lag_mode = true,
            "--supersampling" => supersampling = args.next().unwrap().parse().unwrap(),
            "-H"|"--hdr" => hdr_enabled = true,
            "-b"|"--blur" =>  blur_passes = args.next().unwrap().parse().unwrap(),
            "-E"|"--edge-filter" => edge_filter = true,
            "-h"|"--help" =>
            {
                println!("-d | --demo
-e | --exposure {{float}}
-m | --model {{path}}
-s | --scale {{float}}
-S | --sky {{path}}
-f | --fov {{uint}}
-l | --lag-mode
--supersampling {{uint}}
-H | --hdr
-b | --blur {{uint}}");
                return;
            },
            _ =>
            {
                println!("Uknown argument {}", arg);
                return;
            }
        }
    }

    let relative_input = true;

    if lag_mode
    {
        blur_enabled = false;
    }

    if demo_mode
    {
        blur_enabled = false;
        supersampling = 1;
    }

    let frame_delay = Duration::new(0, 1_000_000_000 / FRAMERATE);

    let win = WindowBuilder::new()
        .with_dimensions(WDIMS.into())
        .with_min_dimensions(WDIMS.into())
        .with_max_dimensions(WDIMS.into())
        .with_maximized(true)
        .with_title("GLium test");

    println!("{:?} Creating context", Instant::now()-init);
    let con = ContextBuilder::new()
        .with_gl(glutin::GlRequest::Specific(
            glutin::Api::OpenGl,
            (3, 2)
            // global type uint requires 3.0
            // out keyword requires 3.0
            // texture function requires 3.0 (#version 130)
            // uniform blocks requires 3.2 (#version 150)
        ))
        .with_gl_profile(GlProfile::Core)
        .with_vsync(false)
        .with_multisampling(4)
        .with_srgb(true);
    let mut events = EventsLoop::new();
    let disp = Display::new(win, con, &events).unwrap();
    println!("OpenGL {}, vendor {}, renderer {}, profile {:?}, min guaranteed GLSL {:?}",
             disp.get_opengl_version_string(),
             disp.get_opengl_vendor_string(),
             disp.get_opengl_renderer_string(),
             disp.get_opengl_profile().unwrap(), // Supported (and guaranteed to return Some) with OpenGL 3.2+
             disp.get_supported_glsl_version());
    //assert!(disp.is_glsl_version_supported(&Version(Api::Gl, 3, 2)));

    println!("{:?} Compiling shaders", Instant::now()-init);
    //let gbprog = load_prog(&disp, ShaderSource::File("assets/shaders/normal.vert"), ShaderSource::File("assets/shaders/deffered1.frag"), None, vec!("PHONG","BLINN")).unwrap();
    let gbprog = load_prog(&disp, ShaderSource::File("assets/shaders/normal.vert"), ShaderSource::File("assets/shaders/deffered1.frag"), None, vec!("PBR"))
        .expect("Deffered shading P1 compilation error");
    /*{
        use std::io::prelude::*;

        let mut vshad = String::new();
        let mut fshad = String::new();
        //let mut gshad = String::new();
        std::fs::File::open("assets/normal.vert").unwrap().read_to_string(&mut vshad).unwrap();
        //std::fs::File::open("assets/explode.geom").unwrap().read_to_string(&mut gshad).unwrap();
        std::fs::File::open("assets/gbuffer.frag").unwrap().read_to_string(&mut fshad).unwrap();
        /*vshad = vshad.replacen("\n", "\n#define MAD\n", 1);
        fshad = fshad.replacen("\n", "\n#define MAD\n", 1);*/
        //Program::from_source(&disp, &vshad[..], &fshad[..], Some(&gshad[..])).unwrap()
        Program::from_source(&disp, &vshad[..], &fshad[..], None).unwrap()
    };*/
    let skyprog = load_prog(&disp, ShaderSource::File("assets/shaders/skybox.vert"), ShaderSource::File("assets/shaders/skybox.frag"), None, vec!())
        .expect("Skybox shaders compilation error");
    //let normprog = load_prog(&disp, ShaderSource::File("assets/shaders/normal.vert"), ShaderSource::File("asssets/shaders/min.frag"), Some(ShaderSource::File("assets/shaders/normals.geom")), vec!()).unwrap();
    let shadprog = load_prog(&disp, ShaderSource::File("assets/shaders/normal.vert"), ShaderSource::File("assets/shaders/identity.frag"), None, vec!("PBR","IBL"))
        .expect("Shadow shaders compilation error");
    //FIXME: alphaprog currently doesnt do any shading
    let alphaprog = load_prog(&disp, ShaderSource::File("assets/shaders/normal.vert"), ShaderSource::File("assets/shaders/alpha.frag"), None, vec!())
        .expect("Alpha shaders compilation error");
    let gblprog = load_prog(&disp, ShaderSource::File("assets/shaders/identity.vert"), ShaderSource::File("assets/shaders/deffered2.frag"), None, if edge_filter {vec!("PBR","IBL","SOBEL")}else{vec!("PBR","IBL")})
        .expect("Deffered shading P2 compilation error");
    let gaussprog = load_prog(&disp, ShaderSource::File("assets/shaders/identity.vert"), ShaderSource::File("assets/shaders/gauss.frag"), None, vec!(if supersampling > 1{"STEP 2"}else{"STEP 1"}))
        .expect("Gaussian blur shaders compilation error");
    let blendprog = load_prog(&disp, ShaderSource::File("assets/shaders/identity.vert"), ShaderSource::File("assets/shaders/blend.frag"), None, vec!())
        .expect("Blending shaders compilation error");
    /*let identityprog = load_prog(&disp, ShaderSource::File("assets/shaders/identity.vert"), ShaderSource::File("assets/shaders/identity.frag"), None, vec!("PBR,IBL"))
        .expect("Identity shaders compilation error");*/

    println!("{:?} Generating models", Instant::now()-init);
    let lightcube =
    {
        let model = Matrix4::from_scale(0.1f32);
        let vertices = &helper::CUBEN_V.iter().map(|vert|model**vert).collect::<Vec<VertexNT>>()[..];
        Vibo::new(&disp, vertices, None::<&[helper::Normal]>, Some(&helper::CUBEN_I))
    };
    let skybox = Vibo::new(&disp, &helper::CUBEN_V, None::<&[helper::Normal]>, Some(&helper::CUBEN_I));
    let avocado = load_gltf(&disp, model, Matrix4::from_scale(scale));
    if !avocado.has_transparency()
    { alpha_enabled = false }
    println!("Alpha support enabled: {}", alpha_enabled);
    let fscreen_quad = Vibo::new(&disp, &helper::SQUARE_V, None::<&[helper::Normal]>, Some(&helper::SQUARE_I));

    println!("{:?} Loading skybox", Instant::now()-init);
    //let cubemap = ["ft","bk","up","dn","rt","lf"].iter().map(|dir| format!("{}_{}.tga", sky, dir));
    let cubemap = ["px","nx","py","ny","pz","nz"].iter().map(|dir| format!("{}{}.png", sky, dir));
    let cubemap = load_cubemap(&disp, cubemap);
    println!("{:?} Convoluting skybox", Instant::now()-init);
    let convoluted = convolute_cubemap(&disp, &cubemap, 64, &skybox, Some(init));
    let convoluted = convoluted.sampled();
    println!("{:?} Prefiltering skybox", Instant::now()-init);
    let prefiltered = prefilter_cubemap(&disp, &cubemap, 256, &skybox);
    let prefiltered = prefiltered.sampled();
    let cubemap = cubemap.sampled().minify_filter(MinifySamplerFilter::LinearMipmapLinear);

    println!("{:?} Convoluting BRDF", Instant::now()-init);
    let integrated = integrate_brdf(&disp, 512, &fscreen_quad);
    let integrated = integrated.sampled().wrap_function(SamplerWrapFunction::Clamp).minify_filter(MinifySamplerFilter::Linear).magnify_filter(MagnifySamplerFilter::Linear);


    let mut light = Light{pos:(-0.5,1.0,1.2), attenuation:(1.0,0.0,0.1), ..Default::default()};

    let mut closed = false;
    let mut fcounter = if demo_mode
    { 500u16 }
    else { 0u16 };

    let sdims = (WDIMS.0*supersampling, WDIMS.1*supersampling);

    let proj: [[f32;4];4] = perspective(Deg(fov), sdims.0 as f32/sdims.1 as f32, 0.1f32, 100f32,).into();

    println!("{:?} Allocating GPU memory", Instant::now()-init);
    //let albedo = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::F16F16F16F16, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let albedo = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::U8U8U8U8, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let gbpos = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::F16F16F16, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let gbnorm = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::F16F16F16, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let gbmr = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::U8U8U8, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let gbem = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::U8U8U8, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let gbdepth = DepthRenderBuffer::new(&disp, DepthFormat::I32, sdims.0, sdims.1).unwrap();
    let mut gbuffer =
    {
        let outputs = [("colortex",&albedo),("postex",&gbpos),("normtex",&gbnorm),("mrotex",&gbmr),("emtex",&gbem)];
        MultiOutputFrameBuffer::with_depth_buffer(&disp, outputs.iter().cloned(), &gbdepth).unwrap()
    };
    let shadmap = DepthTexture2d::empty_with_format(&disp, DepthFormat::F32, MipmapsOption::NoMipmap, 1024, 1024).unwrap();
    let mut shadbuffer = SimpleFrameBuffer::depth_only(&disp, &shadmap).unwrap();
    let shadmap = shadmap.sampled().wrap_function(SamplerWrapFunction::Clamp);

    let lpasshdr = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::F16F16F16, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();

    //let lpassalphatex = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::F16F16F16F16, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let lpassalphatex = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::U8U8U8U8, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let mut alphabuffer =
    {
        //let outputs = [("main_out",&lpassalphatex),("hdr",&lpasshdr)];
        //MultiOutputFrameBuffer::new(&disp, outputs.iter().cloned()).unwrap()
        //MultiOutputFrameBuffer::with_depth_buffer(&disp, outputs.iter().cloned(), &gbdepth).unwrap()
        SimpleFrameBuffer::with_depth_buffer(&disp, &lpassalphatex, &gbdepth).unwrap()
    };
    //let gbalpha = Texture2d::empty(&disp, sdims.0, sdims.1).unwrap();
    //let mut alphabuffer = SimpleFrameBuffer::with_depth_buffer(&disp, &gbalpha, &gbdepth).unwrap();

    let lpasstex = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::F16F16F16F16, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let mut lpassbuffer =
    {
        let outputs = [("main_out",&lpasstex),("hdr",&lpasshdr)];
        MultiOutputFrameBuffer::new(&disp, outputs.iter().cloned()).unwrap()
    };
    let mut skybuffer = SimpleFrameBuffer::with_depth_buffer(&disp, &lpasstex, &gbdepth).unwrap();

    let texgauss = Texture2d::empty_with_format(&disp, UncompressedFloatFormat::F16F16F16, MipmapsOption::NoMipmap, sdims.0, sdims.1).unwrap();
    let mut fbgauss1 = SimpleFrameBuffer::new(&disp, &texgauss).unwrap();
    let mut fbgauss2 = SimpleFrameBuffer::new(&disp, &lpasshdr).unwrap();

    let lpasshdr = lpasshdr.sampled().wrap_function(SamplerWrapFunction::Clamp);

    //let box1 = Drawable{transform:Matrix4::from_scale(0.5f32),geom:&vibo};
    //let box2 = Drawable{transform:Matrix4::from_translation(Vector3::new(-0.9,0.0,-1.3f32))*Matrix4::from_scale(0.5f32),geom:&vibo};
    //let ground = Drawable{transform:Matrix4::from_translation(Vector3::new(0.0,-0.5,0f32))*Matrix4::from_angle_x(Deg(-90f32))*Matrix4::from_scale(10f32),geom:&plane};

    let minimal_dparams = DrawParameters::default();
    let draw_params = DrawParameters
    {
        depth: Depth
        {
            //test: DepthTest::IfLess,
            test: DepthTest::IfLessOrEqual,
            write: true,
            .. Default::default()
        },
        backface_culling: if alpha_enabled { BackfaceCullingMode::CullingDisabled  } else { BackfaceCullingMode::CullClockwise },
        blend: if alpha_enabled { Blend::alpha_blending() } else { Blend::default() },
        .. minimal_dparams.clone()
    };
    let sky_dparams = DrawParameters
    {
        backface_culling: BackfaceCullingMode::CullCounterClockwise,
        blend: Blend::default(),
        .. draw_params.clone()
    };
    let shad_draw_params = DrawParameters
    {
        backface_culling: BackfaceCullingMode::CullCounterClockwise,
        blend: Blend::default(),
        .. draw_params.clone()
    };

    println!("{:?} Done", Instant::now()-init);

    let mut cam_pos = if demo_mode
    { Point3::new(2.0, 1.0, -2f32) }
    else { Point3::new(-3.0, 3.0, 3f32) };

    let mut yaw = 0f32;
    let mut pitch = 0f32;
    let mut cam_dir = -cam_pos.to_homogeneous();

    let mut lag_ring = RingBuffer::<bool>::new(250);

    let mut move_speed_mode = CAM_NORM;
    let mut last_mouse = (0f64, 0f64);

    while !closed
    {
        let before = Instant::now();

        events.poll_events(|ev| match ev
        {
            Event::WindowEvent{event,..} => match event
            {
                CloseRequested => closed=true,
                Resized(LogicalSize{width,height}) if width<1.0 || height<1.0 => std::thread::sleep(frame_delay*9),
                /*Resized(w,h) =>
                {
                    proj = perspective(Deg(90f32), w as f32/h as f32, 0.1f32, 100f32,).into();
                    fbtex = Texture2d::empty(&disp, w, h).unwrap();
                    fbhdr = Texture2d::empty(&disp, w, h).unwrap();
                    depthtex = DepthTexture2d::empty(&disp, w, h).unwrap();
                    let outputs = [("main_out", &fbtex), ("hdr", &fbhdr)];
                    multifb = MultiOutputFrameBuffer::with_depth_buffer(&disp, outputs.iter().cloned(), &depthtex).unwrap();
                },*/
                KeyboardInput{input,..} => if let (false, Some(keycode)) = (demo_mode, input.virtual_keycode)
                {
                    if input.state == ElementState::Pressed
                    {
                        let cam_speed = 0.3f32;
                        match keycode
                        {
                            VirtualKeyCode::Z => cam_pos += cam_speed*move_speed_mode * cam_dir.truncate(),
                            VirtualKeyCode::Q => cam_pos += cam_speed*move_speed_mode * Vector3::new(cam_dir.z, 0.0, -cam_dir.x),
                            VirtualKeyCode::S => cam_pos += cam_speed*move_speed_mode * -cam_dir.truncate(),
                            VirtualKeyCode::D => cam_pos += cam_speed*move_speed_mode * Vector3::new(-cam_dir.z, 0.0, cam_dir.x),
                            VirtualKeyCode::Escape => closed=true,
                            VirtualKeyCode::LShift => move_speed_mode = CAM_FAST,
                            VirtualKeyCode::LControl => move_speed_mode = CAM_SLOW,
                            _ => {}
                        }
                    }
                    else
                    {
                        if input.state == ElementState::Released
                        {
                            match keycode
                            {
                                VirtualKeyCode::LShift
                                    | VirtualKeyCode::LControl => move_speed_mode = CAM_NORM,
                                _ => {}
                            }
                        }
                    }
                },
                _ => {}
            },
            Event::DeviceEvent{event,..} => if let (false, MouseMotion{delta:(ix,iy)}) =  (demo_mode, event)
            {
                let dx: f64;
                let dy: f64;

                if relative_input
                {
                    dx = ix;
                    dy = iy;
                }
                else
                {
                    dx = ix - last_mouse.0;
                    dy = iy - last_mouse.1;
                    last_mouse = (ix, iy);
                }

                let mouse_speed = 0.01f32;
                //cam_angles.0 += dx as f32 * mouse_speed;
                //cam_angles.1 += dy as f32 * mouse_speed;
                yaw += dx as f32 * mouse_speed;
                pitch -= dy as f32 * mouse_speed;
                if pitch > 1.55{pitch=1.55}
                if pitch < -1.55{pitch=-1.55}
                cam_dir.x = pitch.cos() * yaw.cos();
                cam_dir.y = pitch.sin();
                cam_dir.z = pitch.cos() * yaw.sin();
                /*cam_dir = Matrix4::from_angle_x(Deg(dy as f32 * mouse_speed))
                        * Matrix4::from_angle_y(Deg(-dx as f32 * mouse_speed))
                        * cam_dir;*/
            },
            _ => {}
        });

        if closed
        {break;}

        // G-buffer first pass

        gbuffer.clear_color_and_depth((0.0,0.0,0.0,0.0), 1.0);

        let view: [[f32;4];4] = Matrix4::look_at(cam_pos, cam_pos+cam_dir.truncate(), Vector3::unit_y()).into();

        // Model is prescaled
        let identity: [[f32;4];4] = Matrix4::identity().into();
        avocado.draw(&mut gbuffer, &gbprog, &uniform!(proj:proj,view:view,model:identity,fcounter:fcounter), &draw_params, true, false)
            .expect("Failed to draw model in first pass");

		light.pos = (2.0*f32::cos(fcounter as f32*0.003), 0.8+0.2*f32::cos((fcounter%120)as f32*0.0523), 2.0*f32::sin((fcounter%3600) as f32*0.003));
        let model: [[f32;4];4] = (Matrix4::from_translation(light.pos.into()) * Matrix4::from_angle_y(Rad(fcounter as f32*0.00698))).into();
        lightcube.draw(&mut gbuffer, &gbprog, &uniform!(model:model), &draw_params)
            .expect("Failed to draw lightcube in first pass");

        // Normals debug
        //avocado.draw(&mut gbuffer, &normprog, &uniform!(proj:proj,view:view,model:identity), &draw_params, false, false).unwrap();
        //cube1.draw(&mut gbuffer, &normprog, &uniform!(proj:proj,view:view,model:identity), &draw_params).unwrap();

        // Shadow pass
        let mut lightspace = [[0f32;4];4];
        if shadows_enabled
        {
            let view = Matrix4::look_at(light.pos.into(), Point3::new(0.0,0.0,0.0), Vector3::unit_y());
            let proj = ortho(-5.0, 5.0, -3.0, 3.0, 0.5, 10.0);
            //let proj = perspective(Deg(90f32), WDIMS.0 as f32/WDIMS.1 as f32, 0.9f32, 80f32,);
            lightspace = (proj*view).into();
            let view: [[f32;4];4] = view.into();
            let proj: [[f32;4];4] = proj.into();

            shadbuffer.clear_depth(1.0);

            avocado.draw(&mut shadbuffer, &shadprog, &uniform!(model:identity,view:view,proj:proj,fcounter:fcounter), &shad_draw_params, false, false)
                .expect("Failed to draw model's shadows");
        }

        // G-buffer second pass
        lpassbuffer.clear_color(0.0,0.0,0.0,0.0);

        skybox.draw(&mut skybuffer, &skyprog, &uniform!(proj:proj,view:view,cubemap:cubemap), &sky_dparams)
            .expect("Failed to draw skybox");

        //let uniforms = puniform!(&light, &uniform!(lspace:lightspace,postex:&gbpos,normtex:&gbnorm,coltex:&albedo,spectex:&gbspec,shmap:shadmap,camera:[cam_pos.x,cam_pos.y,cam_pos.z],fcounter:fcounter));
        let uniforms = puniform!(&light, uniform!(lspace:lightspace,postex:&gbpos,normtex:&gbnorm,coltex:&albedo,mrotex:&gbmr,emtex:&gbem,shmap:shadmap,
            camera:[cam_pos.x,cam_pos.y,cam_pos.z],fcounter:fcounter,irrmap:convoluted,envmap:prefiltered,brdf_int:integrated));
        fscreen_quad.draw(&mut lpassbuffer, &gblprog, &uniforms, &minimal_dparams)
            .expect("Failed to render opaque lightning pass");

        if alpha_enabled
        {
            lpassalphatex.as_surface().clear_color(0.0,0.0,0.0,0.0);
            // Clearing the entire alpha buffer would also clear the shared HDR (1+) texture
            //alphabuffer.clear_color(0.0,0.0,0.0,0.0);
            avocado.draw(&mut alphabuffer, &alphaprog, &uniform!(model:identity,view:view,proj:proj,fcounter:fcounter), &draw_params, false, true).unwrap();
        }
        gbuffer.clear_color(0.0,0.0,0.0,0.0);
        avocado.draw(&mut gbuffer, &gbprog, &uniform!(model:identity,view:view,proj:proj), &draw_params, true, true)
            .expect("Failed to draw alpha materials in first pass");
        /*fscreen_quad.draw(&mut alphabuffer, &gblprog, &uniforms, &minimal_dparams)
            .expect("Failed to render lightning on alpha materials");*/

        if blur_enabled
        {
            // Gaussian blur is really expansive and its efficiency decreases with resolution as it
            // takes more work to do one pass and more passes are needed if the step is not
            // increased.
            // In the case of supersampling there is no loss of quality with a larger
            // (proportional) step without changing the number of passes
            for _ in 0..blur_passes
            {
                fscreen_quad.draw(&mut fbgauss1, &gaussprog, &uniform!(image:lpasshdr, horizontal:true, pix_step:2.5f32, fcounter:fcounter), &minimal_dparams)
                    .expect("Failed to blur odd pass");
                fscreen_quad.draw(&mut fbgauss2, &gaussprog, &uniform!(image:&texgauss, horizontal:false), &minimal_dparams)
                    .expect("Failed to blur even pass");
            }
        }

        let mut frame = disp.draw();
        if alpha_enabled || blur_enabled || hdr_enabled
        {
            fscreen_quad.draw(&mut frame, &blendprog, &uniform!(tex1:&lpasstex,tex2:lpasshdr,alpha:&lpassalphatex,fcounter:fcounter,exposure:exposure), &minimal_dparams)
                .expect("Failed to blend shaded channels");
        }
        else
        {
            lpasstex.as_surface().fill(&frame, MagnifySamplerFilter::Linear)
        }

        frame.finish().unwrap();

        //angle += 0.005f32;
        if !demo_mode
        { fcounter += 1 }

        if fcounter == std::u16::MAX
        { fcounter = 0 }

        let time_taken = Instant::now() - before;
        let lag = time_taken >= frame_delay;
        lag_ring.add(lag);

        if demo_mode
        {  }
        else if lag == false
        {std::thread::sleep(frame_delay - time_taken)}
        else
        {
            println!("Frame delay exceeded: {}/{}", time_taken.subsec_nanos()/1000_000, frame_delay.subsec_nanos()/1000_000);

            let lagcount = lag_ring.buffer.iter().filter(|x|**x).count();
            if !lag_mode && lagcount > 120
            {
                println!("Lag count during the 180 previous frames: {} -> Activating light mode", lagcount);
                blur_enabled = false;
                shadows_enabled = false;
                lag_mode = true;
                //shadbuffer.clear_depth(1.0);
                //fbgauss2.clear_color(0.0,0.0,0.0,1.0);
            }
        }
    }
}

struct RingBuffer<T>
{
    pub buffer: Vec<T>,
    pos: usize
}

impl<T> RingBuffer<T>
    where T: Clone
{
    pub fn new(capacity: usize) -> Self
    {
        Self
        {buffer: Vec::with_capacity(capacity), pos:0}
    }

    pub fn add(&mut self, item: T) -> Option<T>
    {
        let mut dropped: Option<T> = None;
        if self.pos < self.buffer.len()
        {
            dropped = Some(self.buffer[self.pos].clone());
            self.buffer[self.pos] = item;
        }
        else
        {self.buffer.push(item);}
        self.pos = (self.pos+1)%self.buffer.capacity();

        dropped
    }
}
